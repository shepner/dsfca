#!/bin/sh

# This script is intended to provide a quick(er) way to test this site locally
# using Docker Desktop and then do some clean up afterwords.  This is useful
# when making lots of changes so you dont have to wait forever just to have
# the pipeline say you did it wrong.


# Build and launch app
docker compose up

# URL: http://localhost:8000

# Remove the container
docker compose rm --force

# Remove the image
docker image rm dsfca-web

# Deep clean docker
#docker system prune --force

