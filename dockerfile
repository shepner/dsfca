# Dockerfile used for local development of the site.

FROM python:3.11-buster

WORKDIR /DSFCA_Website

COPY requirements.txt requirements.txt

# use a python venv to install
RUN \
  pip install --upgrade pip && \
  pip install -v -r requirements.txt

#RUN mkdocs build --strict --verbose

EXPOSE 8000

CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"]
