---
source: https://vgl.ucdavis.edu/test/pra-prcd
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Progressive Rod-Cone Degeneration (PRA-prcd)

## Testing

[Progressive Rod-Cone Degeneration (PRA-prcd) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/pra-prcd)

## Information

The material in this section is from: [Progressive Rod-Cone Degeneration (PRA-prcd) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/pra-prcd)

### Phenotype

PRCD affects the photoreceptor cells in the eye involved in both night and day vision. The cells of the retina involved in low light vision, known as rods, are affected first, resulting in night blindness. Subsequently, the bright light photoreceptors known as cones, which are important for color vision, are also affected, resulting in daytime visual deficit. The age of onset and rate of progression vary among breeds, but retinal changes can be identified by screening performed by a veterinary ophthalmologist from adolescence to early adulthood. Most PRCD-affected dogs have noticeable visual impairment by 4 years of age, typically progressing to complete blindness.

### Mode of Inheritance

Autosomal recessive

### Alleles

**N** = Normal, **PRCD** = Progressive rod-cone degeneration

### Breeds appropriate for testing

Many breeds including but not limited to: American Eskimo Dog, American Hairless Terrier, Australian Cattle Dog, Australian Cobberdog, Australian Shepherd, Black Russian Terrier, Barbet, Chesapeake Bay Retriever, Chinese Crested, Chihuahua, Cockapoo, American Cocker Spaniel, Coton de Tulear, English Cocker Spaniel, English Shepherd, Entlebucher Mountain Dog, Field Spaniel, Finnish Lapphund, German Spitz, Giant Schnauzer, Golden Retriever, Golden Doodle, Jack Russell Terrier, Japanese Chin, Lab/Golden Cross, Labradoodle, Australian Labradoodle, Labradoodle/Goldendoodle Cross, Labrador Retriever, Miniature American Shepherd, Norwegian Elkhound, Nova Scotia Duck Tolling Retriever, Pomeranian, Poodle (Standard, Medium, Miniature and Toy), Portuguese Water Dog, Puli, Rat Terrier, Silky Terrier, Schipperke, Spanish Water Dog, Standard Poodle, Swedish Jamthund, Swedish Lapphund, Tibetan Terrier, Xoloitzcuintle, Yorkshire Terrier

### Explanation of Results

-   Dogs with **N/N** genotype will not have this PRCD form of progressive retinal atrophy and cannot transmit this variant to their offspring.
-   Dogs with **N/PRCD** genotype are not expected to be affected by this PRCD form of progressive retinal atrophy, but are carriers. They may transmit this PRCD variant to 50% of their offspring. Matings between two carriers are predicted to produce 25% PRCD-affected puppies.
-   Dogs with **PRCD/PRCD** genotype are expected to develop this PRCD form of progressive retinal atrophy and will transmit this variant to all of their offspring.

[_Results of this test can be submitted to the OFA (Orthopedic Foundation for Animals)_](https://vgl.ucdavis.edu/test-results-ofa)

### Additional Details

Progressive rod-cone degeneration (PRCD) is an inherited form of late-onset progressive retinal atrophy (PRA) that has been identified in many dog breeds. PRCD affects the photoreceptor cells in the eye involved in both night and day vision. The cells of the retina involved in low light vision, known as rods, are affected first, resulting in night blindness. Subsequently, the bright light photoreceptors known as cones, which are also important for color vision, are also affected, resulting in daytime visual deficit. The age of onset and rate of progression vary among breeds, but retinal changes can be identified by screening performed by a veterinary ophthalmologist from adolescence to early adulthood. Most PRCD-affected dogs have noticeable visual impairment by 4 years of age, typically progressing to complete blindness.

PRCD is caused by a single nucleotide change (G>A) at position 5 in the _Progressive rod-cone degeneration_ (_PRCD_) gene that changes the second amino acid from cysteine to tyrosine (C2Y). This amino acid change occurs in a highly conserved region of the protein. The mode of inheritance for this disease is autosomal recessive, which means that males and females are equally affected and that two copies of the mutation are needed to cause PRCD. Exceptionally, a few dogs between 10-13 years of age have been identified that were clinically normal but had two copies of the PRCD mutation. The breeds involved in these cases were American Eskimo, American Cocker Spaniel, English Cocker Spaniel, and Toy Poodle. It has been postulated that genetic modifier(s) that have not yet been identified may play a role in the progression of the disease.

Testing for PRCD assists veterinarians with diagnosis of PRA and helps breeders identify carriers among breeding stock to select appropriate mates that will reduce the risk of producing affected offspring. To avoid the possibility of producing affected puppies, matings between known carriers are not recommended.

Progressive retinal atrophy (PRA) is a medical classification that represents several inherited forms of retinal degeneration that are caused by mutations in different genes. PRCD is one form of PRA. Thus, a normal test result for PRCD (N/N or N/PRCD) does not exclude the possibility that a dog may carry or be affected by another PRA mutation.

## Discussion
