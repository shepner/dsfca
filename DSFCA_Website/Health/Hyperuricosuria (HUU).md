---
source: https://vgl.ucdavis.edu/test/hyperuricosuria
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Hyperuricosuria (HUU)

## Testing

[Hyperuricosuria (HUU) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/hyperuricosuria)

## Information

The material in this section is from: [Hyperuricosuria (HUU) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/hyperuricosuria)

### Phenotype

Hyperuricosuria is characterized by elevated levels of uric acid in the urine and can lead to the formation of bladder/kidney stones.

### Mode of Inheritance

Autosomal recessive

### Alleles

**N** = Normal/Unaffected, **HU** = Hyperuricosuria

### Breeds appropriate for testing

All dog breeds, including American Pitbull Terrier, American Staffordshire Terrier, Australian Shepherd, Black Russian Terrier, Bulldog, Catahoula Leopard Dog, Dalmatian, Danish-Swedish Farmdog, French Bulldog, German Hunting Terrier, German Shepherd, Giant Schnauzer, Jack Russel/Parsons Terrier, Kromfohrländer, Labrador Retriever, Lagotto Romagnolo, Large Munsterlander, South African Boerboel, Spaniel de Pont-Audemer, Swedish Vallhund, Vizsla, Weimaraner

### Explanation of Results

-   Dogs with **N/N** genotype will not have hyperuricosuria and will not transmit this hyperuricosuria variant to their offspring.
-   Dogs with **N/HU** genotype will not have hyperuricosuria, but are carriers. They will transmit this variant to 50% of their offspring. Matings between two carriers are predicted to produce 25% hyperuricosuria-affected puppies.
-   Dogs with **HU/HU** genotype will be affected and are susceptible to develop bladder/kidney stones. They will transmit this hyperuricosuria variant to all of their offspring.

[_Results of this test can be submitted to the OFA (Orthopedic Foundation for Animals)_](https://vgl.ucdavis.edu/test-results-ofa)

### Additional Details

Hyperuricosuria (HUU) means elevated levels of uric acid in the urine. This trait predisposes dogs to form stones in their bladders or sometimes kidneys. These stones often must be removed surgically and can be difficult to treat. HUU is inherited as a simple autosomal recessive defect. A mutation in exon 5 of the gene _Solute carrier family 2, member 9 (SLC2A9)_ has been found to be associated with hyperuricosuria in dogs. HUU can occur in any breed but is most commonly found in the Dalmatian, Bulldog, and Black Russian Terrier. 

A DNA test for the _SLC2A9_ mutation can determine the genetic status of dogs for HUU. Dogs that carry two copies of the mutation will be affected and susceptible to develop bladder/kidney stones. However, the _SCL2A9_ mutation is not the sole cause of urate bladder stones in dogs. Other factors, in addition to genetic test results, such as liver disease and diet need also be considered in clinical evaluation.

## Discussion
