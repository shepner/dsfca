---
source: https://vgl.ucdavis.edu/test/primary-lens-luxation
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Primary Lens Luxation (PLL)

## Testing

[Primary Lens Luxation (PLL) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/primary-lens-luxation)

## Information

The material in this section is from: [Primary Lens Luxation (PLL) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/primary-lens-luxation)

### Phenotype

Primary lens luxation appears spontaneously, typically between 3-8 years, although both eyes are not necessarily affected at the same time. Watery, red, teary eyes may indicate that lens luxation has occurred and veterinary intervention is required.

### Mode of Inheritance

Autosomal recessive

### Alleles

**N** = Normal, **PLL** = Primary lens luxation

### Breeds appropriate for testing

Terrier breeds (many), American Eskimo Dog, Australian Cattle Dog, Chinese Crested, Chinese Foo Dog, Danish-Swedish Farmdog, Italian Greyhound, Lancashire Heeler, Volpino Italiano

### Explanation of Results

-   Dogs with **N/N** genotype will not have primary lens luxation and cannot transmit this variant to their offspring.
-   Dogs with **N/PLL** genotype are at slight risk of developing primary lens luxation and are carriers. They will transmit this variant to 50% of their offspring. Matings between two carriers are predicted to produce 25% primary lens luxation-affected puppies.
-   Dogs with **PLL/PLL** genotype will have or are at risk of developing primary lens luxation, a condition that can lead to blindness if untreated. They will transmit this variant to all of their offspring.

[_Results of this test can be submitted to the OFA (Orthopedic Foundation for Animals)_](https://vgl.ucdavis.edu/test-results-ofa)

### Additional Details

Primary Lens Luxation (PLL) is a painful inherited eye disorder where the lens of the eye moves from its normal position causing inflammation and glaucoma. PLL results from a single base change mutation in the gene _ADAMST17 (ADAMTS17_:c.1473+1). If untreated, the condition can rapidly lead to blindness.

The study identifying this _ADAMTS17_ disease variant looked at 196 dogs from three different breeds (Jack Russell Terrier, Miniature Bull Terrier and Lancashire Heeler) that had been clinically diagnosed with PLL and found that 161 of them had two copies of this variant. Further RNA analysis showed that this _ADAMTS17_ variant resulted in a truncated transcript that was abnormal. Combined, these findings showed a strong association between the _ADAMTS17_:c.1473+1 variant and PLL in all three studied breeds. A subsequent study later identified this _ADAMTS17_ variant in PLL-affected dogs of various other breeds. However, 12 dogs diagnosed with PLL in the original study did not have this disease variant, indicating that while this _ADAMTS17_ variant is a common cause for PLL in several dog breeds, it is not the only one. 

The disease is inherited in an autosomal recessive fashion thus both sexes are equally affected. PLL appears spontaneously, typically between 3-8 years, although both eyes are not necessarily affected at the same time. Watery, red, teary eyes may indicate that lens luxation has occurred and veterinary intervention is required. Dogs with only one copy of the disease mutation most frequently show no sign of the disease but can, on occasion, develop PLL. Breeding between two carriers is expected to produce 25% affected offspring.

Testing for PLL assists owners and breeders in identifying affected and carrier dogs. Breeders can use results from the test as a tool for selection of mating pairs to avoid producing affected dogs.

## Discussion
