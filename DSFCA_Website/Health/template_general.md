---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and add tags as a list below
---

**Navigation**

- Parent:: 
- Peer:: 
- Child:: 

# {{title}}

!!! WARNING "This is a template so update as appropriate"

The navigation section up top is really optional but it should (or should not) be for the entire site.  The formatting is what ExcaliBrain (a concept mapping tool) supports which is a comma delimited list of links.  For the sanity of updating the it is recommended that you have a comma and space `, ` after each link.

!!! NOTE "If you dont use the Navigation section, you *must* use tags!"

    Without using either means you must rely upon mkdoc's navigation system which is painful to manage and even worse when using Obsidian.  Tag support in Mkdocs is good.  Use it regardless.

It is important that the filename matches the title.  If you need a subdirectory (ie when you have the `_resources` directory), also name the directory the same as the file.  This practice will help ensure the dir and website structure is orderly and manageable.

This is a paragraph with a Markdown footnote example[^1][^2]

[^1]: Note that Markdown links **MUST** have some plain text between them and the footnote marker in order to work.
[^2]: Also note that it is highly recommended that you keep the footnote text close to the item referencing it.  This is extremely helpful when you need to ensure the text and footnote are in sync with each other, not orphaned, etc.

This is a ***new*** paragraph with *some* additional <u>formatting</u> examples.  There are more throughout.

Be sure to place a blank line between paragraphs, title blocks, etc.  You will end up with a jumbled mess otherwise.

Just because it looks good in Obsidian doesnt mean it will look good in Mkdocs.  Obsidian tends to be extremely forgiving.  Mkdocs is not.  Same goes for links.  If your MR fails the test, chances are you have a broken link.  Search the job's output for `warning` and fix whatever its complaining about.

<details><summary>Click to display this hidden text example</summary>

Here is some Markdown text that is hidden within a HTML block.  **Notice**: In order for the *formatting* to work, there needs to be a blank line between the HTML and the Markdown

```
codeblock
example
here
```

Some more random text

</details>

Notice that this text is *not* hidden

When embedding images, Obsidian will maintain the URLs for markdown based links.  However you do not have any formatting:

![](_resources/template_general/image123.png)

If you must have formatting, you *must* use HTML and Obsidian will NOT maintain the links for you:

<img src="_resources/template_general/img_girl.jpg" alt="Girl in a jacket" width="200" height="200">

In the above examples, please note that the files are located in `_resources/<md filename>/<filename>`.  **Make sure** that you maintain that type of structure within the site as this will help keep the directory structure manageable.  You may have to manually move files by hand if they end up in the wrong location.  ==This is not difficult up front but can become a monumental task after the fact.==
