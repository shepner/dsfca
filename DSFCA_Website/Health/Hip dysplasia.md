---
source: !
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Hip dysplasia

## Testing

tbd

## Discussion

[Nyheter och information | gardshund.com](https://gardshund-com.translate.goog/avel/nyheter-och-information-37611484?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

![](_resources/Pasted%20image%2020230331195931.png)

---

Regarding breeder informed decisions on hip testing Farmdogs…

I was able to get permission to share this study with you. The study was done for the Finnish Farmdog Association. There are 2445 Farmdogs registered in Finland out of Danish and Swedish lines. They studied 1124 dogs whelped in Finland with the following outcomes. Source: Mia Satamo

![](_resources/Pasted%20image%2020230331201631.png)

![](_resources/Pasted%20image%2020230331201640.png)

