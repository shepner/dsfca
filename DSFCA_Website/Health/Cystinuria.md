---
source: https://www.vet.cornell.edu/departments-centers-and-institutes/riney-canine-health-center/health-info/cystinuria
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

Source: [Cystinuria | Cornell University College of Veterinary Medicine](https://www.vet.cornell.edu/departments-centers-and-institutes/riney-canine-health-center/health-info/cystinuria)

# Cystinuria

## Overview

Cystinuria is an uncommon, inherited condition that causes an amino acid called cystine to build up in urine. Cystine can be excreted in urine and lead to the formation of bladder or kidney stones. Diagnosis and veterinary management of this condition in dogs can help avoid painful and potentially dangerous complications. 

### Fast facts

-   Inherited condition that can lead to bladder or kidney stones 
-   Diagnosed with urine tests and bladder imaging 
-   Managed with prescription diets and stone removal 
-   Type III may be cured with neutering 
-   Stones often recur, but formation decreases with age 

## Causes

Cystine is a type of amino acid in the body that is normally reabsorbed by the kidneys. Cystinuria occurs when the kidneys are not able to properly reabsorb cystine, causing it to accumulate in the urine and form bladder or kidney stones.  

There are three types of cystinuria, two of which can occur in males and females, and one that is influenced by the presence of sex hormones common in intact males. 

While both males and females with cystinuria are equally affected from excess cystine in their urine, the obstruction of urine flow is more common in males due to differences in their anatomy.  

## Clinical signs

Dogs with cystinuria may not show any signs unless bladder or kidney stones form. The most common signs include: 

-   Straining to urinate 
    
-   Frequent, small amounts of urination 
    
-   Accidents in the house  
    
-   Blood in the urine 
    

The inability to urinate may also occur if stones develop and cause an obstruction in the urethra, which is a medical emergency. The signs for this condition will be much more severe — including a large and firm bladder, belly pain, vomiting and lethargy. If left untreated, a urinary obstruction can be fatal. 

## Diagnosis

A test called a urinalysis will be performed to look for the presence of cystine crystals, the pH of the urine and any coexisting issues, such as a urinary tract infection. Cystine crystals form in acidic urine (which has a lower pH).  

Another urine test called urine nitroprusside can screen for cystinuria. Specialized X-rays, ultrasounds or other urinary imaging tests may be used to diagnose cystine stones in the bladder or kidneys.  

## Treatment

The goal of treatment is to reduce the amount of cystine excreted in the dog’s urine, dissolve what cystine remains and avoid stone formation.  

Cystine can dissolve if the urine is made less acidic (by increasing its pH). This is achieved by feeding a prescription diet with reduced sodium and protein — particularly targeting an amino acid called methionine, which is one of the precursors involved in forming cystine stones.  

The urine should become diluted by feeding your dog a canned diet, adding water to their meals and encouraging them to drink more water. Medications may be needed if diet alone does not increase the urine pH enough to dissolve the cystine.  

After bladder stones develop, it is necessary to remove the stones and manage any secondary urinary tract infections or additional irritation. Removal often requires surgery. Neutering intact male dogs may be curative for certain types of androgen-dependent (sex hormone) cystinuria.   

Genetic testing is available for a few breeds known to be affected by cystinuria. And since cystinuria can be inherited, dogs suspected of having (or carrying) cystinuria should not be used for breeding without genetic testing and careful consideration of mate selection. 

## Outcome

Cystine stones commonly recur within 6-12 months if they are surgically removed and no changes are made in the diet. Even with dietary modifications, cystine stones can still recur at some point in a dog’s life, but the rate of stone formation tends to decrease with age.  

Your veterinarian will set a schedule for monitoring your dog for recurrence of clinical cystinuria by checking their urine or performing an X-ray or ultrasound to check for stones. This will allow your veterinarian to ensure that the dietary therapy is well-managed and help adjust your dog’s treatment if needed.  

## Genetics

Cystinuria is typically divided into three types. Type I occurs in Labrador Retrievers and Newfoundlands, each with a different variant of the same gene, called SLC3A1. These are both recessive variants, meaning that  two copies of the variant are needed for your dog to be considered at risk from the variant. 

Type II is caused when the inheritance is dominant, meaning only one copy of the variant is needed to cause the condition. Type II cystinuria can occur from a variant of two separate genes. In Australian Cattle Dogs, it is due to a variant in the SLC3A1 gene (Type II-A), and in Miniature Pinschers, it is due to a variant in the SLC7A9 gene (Type II-B). 

Type III is associated with the hormones of intact male dogs (making it androgen-dependent). A variety of breeds may be genetically at-risk, though the causative variants have not yet been discovered. Type III cystinuria in Mastiffs, English Bulldogs and French Bulldogs has a few linked markers that are recessively inherited genetic variants involving both the SLC3A1 and SLC7A9 genes.

## DSF specific

This information is from the Swedish Farmdog Club: [Nyheter och information | gardshund.com](https://gardshund-com.translate.goog/avel/nyheter-och-information-37611484?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

### Status report Cystenuria November 2020  

**Cystinuria in a Danish-Swedish farm dog**

Cystinuria is a disease that often leads to recurrent urinary stone formation. The reason is that the return transport in the kidneys from urine to blood of a special amino acid, cystine, does not work. This leads to increased amounts of cystine in the urine. Cystine has low solubility in acidic urine and crystals and stones form. This can lead to blockage of the ureters with severe pain, kidney failure and, in the worst case, death as a result. There is still no cure for cystinuria, but with the help of special feed and medication it is possible to reduce the re-formation of cystine stones and even in some cases to dissolve existing crystals. For affected male dogs, castration is recommended, which usually results in reduced stone formation. If the dog has been diagnosed with cystinuria, lifelong treatment applies.

The genetic background of cystinuria has been elucidated in some breeds. The disease is divided into four classes;

Type IA: Caused by a mutation in the SLC3A1 gene and has an autosomal recessive pattern of inheritance.

Type II-A: Also caused by a mutation in the SLC3A1 gene but has an autosomal dominant inheritance pattern.

Type II-B: Caused by a mutation in the SLC7A9 gene and inherited in an autosomal dominant manner.

Type III: Here mutation and inheritance are still unknown. Type III is hormone dependent and affects exclusively intact adult male dogs.

At the beginning of February, a number of Danish-Swedish farm dogs with cystinuria, some healthy control animals and healthy close relatives were tested for the mutation in the SLC7A9 gene that causes cystinuria in several breeds. All dogs were negative for this mutation. The tests have been done in collaboration with Laboklin and a veterinary medical research group that works with cystinuria. Danish-Swedish farm dogs have previously tested negative for the two known mutations in the SLC3A1 gene that cause cystinuria in some other breeds. It is now completely established that cystinuria in Danish-Swedish farm dogs is not caused by any of the known mutations.

The breed club's intention is now to go ahead and seek funds to be able to be part of a project on hormone-dependent cystinuria (cystinuria type III) which occurs, among other things, in the Kromfohrländer and Irish terrier breeds. This type of cystinuria can only develop in the presence of the male dog's sex hormones and only affects intact male dogs. The inheritance is still unknown but is suspected to be complex or autosomal recessive with probable environmental influence. The research is difficult because bitches never get this type of cystinuria and the males are probably just predisposed, ie not even all genetically loaded males get sick. The aim of this project is to be able to develop a genetic test to be able to identify predisposed dogs and carriers. Since all cases of the disease that have occurred so far in Danish-Swedish farm dogs are intact males, the researchers' assessment is that it is very likely this type of cystinuria that affects Danish-Swedish farm dogs and we are welcome to participate in the project. The project is led by Prof. Dr. Urs Giger and Prof. Dr. Tosso Leeb.

Prof. Dr. Urs Giger is a veterinarian, researcher and world-leading expert in hereditary metabolic diseases in domestic animals. He has researched cystinuria on a genetic, biochemical and clinical level for many years and has, among other things, developed genetic tests for cystinuria for several dog breeds. Prof. Dr. Tosso Leeb is an expert in molecular genetics and DNA sequence analysis.

We are also in contact with Prof. Åke Hedhammar, veterinarian and researcher at SLU and veterinary medical advisor at SKK for advice regarding participation in this research project.

Cystinuria in Danish-Swedish farm dogs is still considered a disease of unknown inheritance and SKK's breeding recommendations continue to apply:

· Do not use dogs with cystinuria in breeding.  
· Do not reproduce the same parental combination that left offspring with cystinuria.  
· Be careful when breeding siblings, parents and offspring of sick dogs.  
· In addition to this, it is recommended to breed with a low inbreeding rate to reduce the risk of any recessive disease predisposition doubling.

It is of course also very important that breeders and male dog owners are open and inform each other about cases of cystinuria in their lines to avoid possible duplication of disease predisposition.

We have only become aware of one new case of cystinuria in DSG in 2020.

We ask for continued help with reporting cases of cystinuria to the Breed Club for mapping relationships between affected animals and for possible future sampling. If you have a dog affected by cystinuria, notify the breed club via e-mail to the group for breeding issues; [arbetsgruppavel@gardshund.com](mailto:arbetsgruppavel@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) .

Don't forget to send a veterinary certificate.

### Status report April 2020 - Cystinuria at DSG

At the beginning of February, a number of Danish-Swedish farm dogs with cystinuria, some healthy control animals and healthy close relatives were tested for the mutation in the SLC7A9 gene that causes cystinuria in several breeds. All dogs were negative for this mutation.  
The tests have been done in collaboration with Laboklin and a veterinary medical research group that works with cystinuria. Danish-Swedish farm dogs have previously tested negative for the two known mutations in the SLC3A1 gene that cause cystinuria in some other breeds. It is now completely established that cystinuria in Danish-Swedish farm dogs is not caused by any of the known mutations.  
  
The breed club's intention is now to go ahead and seek funds to be able to be part of a project on hormone-dependent cystinuria (cystinuria type III) which occurs, among other things, in the Kromfohrländer and Irish terrier breeds. This type of cystinuria can only develop in the presence of the male dog's sex hormones and only affects intact male dogs.  
The inheritance is still unknown but is suspected to be complex or autosomal recessive with probable environmental influence.  
The research is difficult because bitches never get this type of cystinuria and the males are probably just predisposed, ie not even all genetically loaded males get sick.  
The purpose of this project is to be able to develop a genetic test to be able to identify predisposed dogs and carriers. Since all cases of the disease that have occurred so far in Danish-Swedish farm dogs are intact males, the researchers' assessment is that it is very likely this type of cystinuria that affects Danish-Swedish farm dogs and we are welcome to participate in the project. The project is led by Prof. Dr. Urs Giger and Prof. Dr. Tosso Leeb. Prof. Dr. Urs Giger is a veterinarian, researcher and world-leading expert in hereditary metabolic diseases  in pets. He has researched cystinuria on a genetic, biochemical and clinical level for many years  and has, among other things, developed genetic tests for cystinuria for several dog breeds. Prof. Dr. Tosso Leeb is an expert in molecular genetics and DNA sequence analysis.

We are also in contact with Professor Åke Hedhammar, veterinarian and researcher at SLU and veterinary medical  
advisor at SKK for advice regarding participation in this research project.  
  
Cystinuria in Danish-Swedish farm dogs is still considered a disease of unknown inheritance, and SKK's breeding recommendations continue to apply:

- Do not use dogs with cystinuria in breeding.  
- Do not repeat the same parental combination that left offspring with cystinuria.  
- Be careful when breeding siblings, parents and offspring of sick dogs.  
- In addition to this, it is recommended to breed with a low inbreeding rate to reduce the risk of any recessive disease predisposition doubling.

It is of course also very important that breeders and male dog owners are open and  
inform each other about cases of cystinuria in their lines to avoid possible duplication of disease predisposition.  
  
We ask for continued help with reporting cases of cystinuria to the Breed Club for  
mapping relationships between affected animals and for possible future sampling.  
If you have a dog affected by cystinuria, notify the breed club via e-mail to the group for breeding issues; [arbetsgruppavel@gardshund.com](mailto:arbetsgruppavel@gardshund.com.?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) [.](mailto:arbetsgruppavel@gardshund.com.?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)   Don't forget to send a veterinary certificate.

## Discussion

The [Swedish Farmdog Club](http://gardshund.com/) is conducting a [study](https://gardshund-com.translate.goog/avel/h%C3%A4lsa?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) regarding this:

>We are currently conducting a study on the relationship between cystinuria and feed and if you want to participate, you can email the breeding group, [**_arbetsgruppavel@gardshund.com_**](mailto:arbetsgruppavel@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

