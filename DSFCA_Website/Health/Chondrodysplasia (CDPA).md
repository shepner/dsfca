---
source: https://vgl.ucdavis.edu/test/cddy-cdpa
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Chondrodysplasia (CDPA)

## Testing

[Chondrodystrophy (CDDY and IVDD) and Chondrodysplasia (CDPA) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/cddy-cdpa)

## Information

The material in this section is from: [Chondrodystrophy (CDDY and IVDD) and Chondrodysplasia (CDPA) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/cddy-cdpa)

### Phenotype

Dogs with chondrodysplasia (CDPA) have short legs; this phenotype is characteristic of many breeds such as Corgis and Dachshunds. Chondrodystrophy (CDDY), caused by a separate mutation, also includes a short-legged phenotype as well as abnormal premature degeneration of intervertebral discs (also referred to as intervertebral disc disease, IVDD). These degenerated discs are susceptible to herniation. Multiple factors are thought to influence whether a particular disc herniates in an individual dog. 

### Mode of Inheritance

_Chondrodysplasia (CDPA):_ Autosomal dominant  
_Chondrodystrophy (CDDY):_ Autosomal dominant for intervertebral disc disease, semi-dominant for height

### Alleles

**N** = Normal, **CDPA** = Chondrodysplasia, **CDDY** = Chondrodystrophy

### Breeds appropriate for testing

Alpine Dachsbracke, American Cocker Spaniel, Australian Shepherd, Basset Hound, Bavarian Mountain Hound, Beagle, Bichon Frise, Boykin Spaniel, Cardigan Welsh Corgi, Cavalier King Charles Spaniel, Chesapeake Bay Retriever, Chihuahua, Chinese Crested, Clumber Spaniel, Coton de Tulear, Dachshund, Dandie Dinmont Terrier, Danish Swedish Farmdog, English Springer Spaniel, Entlebucher Mountain Dog, French Bulldog, German Hound, Havanese, Jack Russell Terrier, Maltese, Pekingese, Pembroke Welsh Corgi, Pinscher (Miniature), Poodle (Miniature and Toy), Poodle (Standard), Portuguese Water Dog, Pug, Rat Terrier, Russian Tsvetnaya Bolonka, Schweizer Laufhund, Schweizerischer Niederlaufhund, Scottish Terrier, Sealyham Terrier, Shih Tzu, Skye Terrier, Yorkshire Terrier

_(This is not a complete list of breeds. Research on the distribution of this mutation across breeds is ongoing)_

### Explanation of Results

**Chondrodysplasia (CDPA):**

-   Dogs with **N/N** genotype will not have this form of chondrodysplasia, which causes the short-legged phenotype of certain dog breeds, and cannot transmit this chondrodysplasia variant to their offspring.
-   Dogs with **N/CDPA** genotype will have leg shortening compared to N/N dogs. They will transmit this CDPA variant to 50% of their offspring. Matings with N/N dogs are predicted to produce 50% puppies with shortened legs.
-   Dogs with **CDPA/CDPA** genotype will have leg shortening compared to N/N dogs and will transmit this chondrodysplasia variant to all of their offspring. Matings with any genotype are predicted to produce all puppies with shortened legs.

**Chondrodystrophy (CDDY)**

-   Dogs with **N/N** genotype do not have this chondrodystrophy variant and therefore are not predicted to be at increased risk for intervertebral disc disease. They cannot transmit this chondrodystrophy variant to their offspring.
-   Dogs with **N/CDDY** genotype will have leg shortening compared to N/N dogs and intervertebral disc disease, and are at risk for intervertebral disc herniation. They will transmit this CDDY variant to 50% of their offspring. Matings with N/N genotype dogs are predicted to produce 50% shorter-legged puppies at risk for intervertebral disc herniation.
-   Dogs with **CDDY/CDDY** genotype will have leg shortening compared to N/N dogs and intervertebral disc disease, and are at risk for intervertebral disc herniation. If a CDDY/CDDY dog is bred, all of the puppies in the litter will have shorter legs, intervertebral disc disease and will also be at risk for intervertebral disc herniation, regardless of the mate's genotype.

[_Results of this test can be submitted to the OFA (Orthopedic Foundation for Animals)_](https://vgl.ucdavis.edu/test-results-ofa)

### Additional Details

Shorter legs in dogs are explained by two retrogene insertions of functional fibroblast growth factor 4 (FGF4). FGF4 gene is involved in many biological processes including bone development.

The first insertion discovered (Parker et al. 2009) is an FGF4-retrogene insertion in dog chromosome 18 (FGF4-18). This FGF4-18 insertion explains a short-legged phenotype known as chondrodysplasia (CDPA) in breeds such as Basset Hound, Pembroke Welsh Corgi, Dachshunds, West Highland White Terriers and Scottish Terriers. CDPA inheritance is considered to follow an autosomal dominant mode.

The Chondrodystrophy (CDDY) mutation was discovered by researchers in the Bannasch Laboratory at the University of California, Davis (Brown et al. 2017) as a second FGF4-retrogene insertion in dog chromosome 12. CDDY includes a short-legged phenotype and abnormal premature degeneration of intervertebral discs leading to susceptibility to Hansen’s type I intervertebral disc disease (IVDD). The intervertebral disc, which sits between vertebrae, is composed of an outer fibrous basket (annulus fibrosus) made of 70% collagen and an inner gel-like layer called the nucleus pulposus. These structures allow for flexibility of the vertebral column. In Chondrodystrophic breeds, premature calcification of the nucleus pulposus at early age (from birth to 1 year of age) results in degeneration of all discs in young dogs. These abnormal discs are predisposed to herniation into the spinal canal where the inflammation, and hemorrhage can cause severe pain and neurological dysfunction (myelopathy) termed Intervertebral Disc Disease or IVDD. IVDD has high mortality rate and high cost of surgical and medical veterinary care.

CDDY is inherited as a semi-dominant trait for height, meaning that dogs with 2 copies of the mutation are smaller than dogs with only 1 copy. With respect to IVDD, the inheritance follows a dominant mode, meaning that 1 copy of the FGF4-12 mutation is sufficient to cause disc degeneration and predispose dogs to disc herniation. Dogs that have both FGF4-12 and FGF4-18 show a more drastic reduction of leg length.

The Veterinary Genetics Laboratory offers a combined test for CDDY and CDPA for breeds that have long and short leg phenotypes. Our tests assay for the causal variants, not for linked markers, therefore the genotypes are accurate and not inferred. CDDY and CDPA occur in many breeds. Testing for these mutations can help breeders determine if CDDY is present among breeding stock and to identify dogs at risk for IVDD. In breeds where both mutations are present, breeders can benefit from test results to implement breeding strategies to reduce incidence of CDDY, while retaining the short-legged phenotype conferred by CDPA.

The CDDY variant has been found in many breeds such as **Alpine Dachsbracke, American Cocker Spaniel, Australian Shepherd, Basset Hound, Bavarian Mountain Hound, Beagle, Bichon Frise, Boykin Spaniel, Cardigan Welsh Corgi, Cavalier King Charles Spaniel, Chesapeake Bay Retriever, Chihuahua, Chinese Crested, Clumber Spaniel, Coton de Tulear, Dachshund, Dandie Dinmont Terrier, Danish Swedish Farmdog, English Springer Spaniel, Entlebucher Mountain Dog, French Bulldog, German Hound, Havanese, Jack Russell Terrier, Maltese, Nova Scotia Duck Tolling Retriever, Pekingese, Pembroke Welsh Corgi, Pinscher (Miniature), Poodle (Miniature and Toy), Poodle (Standard), Portuguese Water Dog, Pug, Rat Terrier, Russian Tsvetnaya Bolonka, Schweizer Laufhund, Schweizerischer Niederlaufhund, Scottish Terrier, Sealyham Terrier, Shih Tzu, Skye Terrier, and Yorkshire Terrier.** This is not a complete list of breeds.

A comprehensive study was recently completed by Batcher and colleagues 2019 to investigate the breed distribution. ([https://doi.org/10.3390/genes10060435](https://doi.org/10.3390/genes10060435)) In addition, the VGL has conducted a study of nearly 4000 dogs across 35 breeds to identify allele frequencies and make recommendations on testing. These data can be found in the following table, we will update this table periodically as more dogs are tested.

!!! NOTE  "The table can be found [here](https://vgl.ucdavis.edu/test/cddy-cdpa)"

**Recommendations on Testing:** For those breeds in which CDDY has been documented to segregate, in other words, both the CDDY allele and the normal allele are present in the breed, genetic testing is recommended. The results of this test can assist in breeding decisions and should be shared with the animal’s veterinarian to assist in clinical decisions. The CDDY allele has been found to be present in mixed breed dogs and therefore testing is recommended for these animals as well. For breeds where the CDDY allele is fixed in the population, in other words only the CDDY is present or allele frequency is 1.0, genetic testing for CDDY is not necessary, in these cases it is reasonable to assume that all dogs in that breed will be homozygous for CDDY (CDDY/CDDY).

_For more information on recommendations on genetic testing and counseling, please see [this article written by Drs. Bannasch and Bellone](https://vgl.ucdavis.edu/news/managing-genetics-chondrodystrophy)._

## Discussion
