---
source: https://vgl.ucdavis.edu/test/degenerative-myelopathy
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Degenerative Myelopathy (DM)

## Testing

[Degenerative Myelopathy (DM) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/degenerative-myelopathy)

## Information

The material in this section is from: [Degenerative Myelopathy (DM) | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/degenerative-myelopathy)

### Phenotype

Affected dogs usually present clinical signs of disease in adulthood (at least 8 years of age) with gradual muscle wasting and loss of coordination typically beginning in the hind limbs. Disease progression continues until the dog is unable to walk. Small breed dogs tend to progress more slowly. In late stages of the disease, dogs may become incontinent and the forelimbs may be affected. Affected dogs may fully lose the ability to walk 6 months to 2 years after the onset of signs.

### Mode of Inheritance

Autosomal recessive, incomplete penetrance

### Alleles

**N** = Normal/Unaffected, **DM** = Degenerative myelopathy

### Breeds appropriate for testing

Many breeds

### Explanation of Results

-   Dogs with **N/N** genoytpe will not have degenerative myelopathy and cannot transmit this variant to their offspring.
-   Dogs with **N/DM** genotype will not have degenerative myelopathy as a result of this genetic variant, but are carriers. They will transmit this variant to 50% of their offspring. Matings between two carriers are predicted to produce 25% degenerative myelopathy-affected puppies.
-   Dogs with **DM/DM** may have degenerative myelopathy, a disabling condition, and will transmit this variant to all of their offspring.

[_Results of this test can be submitted to the OFA (Orthopedic Foundation for Animals)_](https://vgl.ucdavis.edu/test-results-ofa)

### Additional Details

Degenerative myelopathy (DM) is an inherited neurologic disorder of dogs similar to Lou Gehrig’s disease in humans. Affected dogs usually present clinical signs of disease in adulthood (at least 8 years of age) with gradual muscle wasting and loss of coordination that typically begins in the hind limbs because of nerve degeneration. Disease progression continues until the dog is unable to walk. Small breed dogs tend to progress more slowly. In late stages of the disease, dogs may become incontinent and the forelimbs may be affected. Affected dogs may fully lose the ability to walk 6 months to 2 years after the onset of signs.

The disease is inherited in an autosomal recessive fashion with incomplete penetrance, and is caused by a mutation (c.118G>A) in the gene _SOD1_. Thus, two copies of the _SOD1_ mutation (DM/DM) confer increased risk for DM but not all DM/DM dogs across breeds will develop the disease. The variable presentation between breeds suggests that other genetic and environmental factors play a role in disease expression. There is ongoing research to identify other genetic factors that modify risk for DM in different breeds. In addition, similar disease presentation is observed in some animals that do not have the _SOD1_ mutation.

Genetic testing for the _SOD1_ (c.118G>A) mutation, reported by the VGL as DM, helps breeders establish the genetic status of breeding stock and select mating pairs appropriately to reduce the risk of producing DM-affected offspring. Breeding two carriers of the _SOD1_ mutation together is predicted to produce 25% pups that may develop DM.

Based on a 2014 study by Zeng and colleagues, the _SOD1_ mutation (c.118G>A) has been detected in at least 124 dog breeds to date. Clinical signs of DM with confirmed histopathologic findings and a homozygous DM/DM genotype has been reported in mixed breed dogs and dogs from the following breeds: American Eskimo Dog, Australian Shepherd, Bernese Mountain Dog, Bloodhound, Borzoi, Boxer, Cardigan Welsh Corgi, Cavalier King Charles Spaniel, Chesapeake Bay Retriever, English Springer Spaniel, German Shepherd, Golden Retriever, Kerry Blue Terrier, Pembroke Welsh Corgis, Pug, Rhodesian Ridgeback, Rough Collie, Soft Coated Wheaten Terrier, Standard Poodle, and Wire Fox Terrier.

## Discussion
