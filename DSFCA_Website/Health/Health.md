---
source: !
tags:
- Health
---

**Navigation**

- Parent:: [README](../README.md), 
- Peer:: [Breeding strategy](../Breeding%20strategy.md), 
- Child:: 

# Health

This page is to help provide resources to make this topic easier for breeders and prospective owners.

While Danish-Swedish Farmdogs are generally healty and there is no *mandated* testing, there are some things to that would be best kept out of the bloodlines.  Any animal diagnosed with any of the following must not be used for breeding.

| Name                                                                                                  | [Occurrence](../Occurrence.md) | Breeders: Recommend to test? |
| ----------------------------------------------------------------------------------------------------- | ------------------------------ |:----------------------------:|
| [Chondrodysplasia (CDPA)](Chondrodysplasia%20(CDPA).md)                                               | Very uncommon?                 |                              |
| [Chondrodystrophy (CDDY)](Chondrodystrophy%20(CDDY).md)                                               | Uncommon?                      |             TRUE             |
| [Degenerative Myelopathy (DM)](Degenerative%20Myelopathy%20(DM).md)                                   | Not expected?                  |                              |
| [Hip dysplasia](Hip%20dysplasia.md)                                                                   | Uncommon?                      |             TRUE             |
| [Hyperuricosuria (HUU)](Hyperuricosuria%20(HUU).md)                                                   | Very uncommon?                 |                              |
| [Natural Bobtail](Natural%20Bobtail.md)                                                               | Frequent?                      |             TRUE             |
| [Primary Lens Luxation (PLL)](Primary%20Lens%20Luxation%20(PLL).md)                                   | Uncommon?                      |             TRUE             |
| [Progressive Rod-Cone Degeneration (PRA-prcd)](Progressive%20Rod-Cone%20Degeneration%20(PRA-prcd).md) | Very uncommon?                 |                              |
| [Luxating Patella](Luxating%20Patella.md)                                                             | Uncommon?                      |             TRUE             |
| [Eclampsia](Eclampsia.md)                                                                             | ??                             |              ??              |
| [Cystinuria](Cystinuria.md)                                                                           | ??                             |              ??              |
