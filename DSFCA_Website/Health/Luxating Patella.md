---
source: https://en.wikipedia.org/wiki/Luxating_patella
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

Excerpted from: [Luxating patella - Wikipedia](https://en.wikipedia.org/wiki/Luxating_patella)

# Luxating Patella

A **luxating patella**, sometimes called a **trick knee**, is a condition in which the [patella](https://en.wikipedia.org/wiki/Patella "Patella"), or kneecap, dislocates or moves out of its normal location. It can be associated with damage to the [anterior cruciate ligament](https://en.wikipedia.org/wiki/Anterior_cruciate_ligament "Anterior cruciate ligament").[^1]

[^1]: See ["The Knee and Shoulder Centers - [PRINTABLE] Anterior Cruciate Ligament Surgery"](http://www.kneeandshoulder.md/print/print_acl.html). Retrieved 2007-11-27.

Patellar luxation is a common condition in [dogs](https://en.wikipedia.org/wiki/Dog "Dog"), particularly small and miniature breeds. The condition usually becomes evident between the ages of 4 and 6 months. It can occur in [cats](https://en.wikipedia.org/wiki/Cat "Cat"), as well, especially [domestic short-haired cats](https://en.wikipedia.org/wiki/Domestic_short-haired_cat "Domestic short-haired cat").[^2]

[^2]: See Ettinger, Stephen J.; Feldman, Edward C. (1995). _Textbook of Veterinary Internal Medicine_ (4th ed.). W.B. Saunders Company. [ISBN](https://en.wikipedia.org/wiki/ISBN_(identifier) "ISBN (identifier)") [0-7216-6795-3](https://en.wikipedia.org/wiki/Special:BookSources/0-7216-6795-3 "Special:BookSources/0-7216-6795-3").

## Causes

Rarely, it can be caused by some form of [blunt trauma](https://en.wikipedia.org/wiki/Blunt_force_trauma "Blunt force trauma"), but most frequently, it is a developmental, [congenital](https://en.wikipedia.org/wiki/Congenital "Congenital") defect. In congenital cases, it is frequently bilateral. The condition can also be inherited through genetics. This can also be caused by obesity.

## Diagnosis

Diagnosis is made through palpation of the knee, to see whether it slips inside the joint more than would normally be expected. Often, a dog owner might be told that his or her pet has "loose knee", but this is not a medical term, and it is not correct to use it interchangeably with luxating patella.[^4a]

[^4a]: See ["Patellar Luxation"](https://web.archive.org/web/20070826115200/http://www.offa.org/patluxinfo.html). [Orthopedic Foundation for Animals](https://en.wikipedia.org/wiki/Orthopedic_Foundation_for_Animals "Orthopedic Foundation for Animals"). Archived from [the original](http://www.offa.org/patluxinfo.html) (text/html) on 2007-08-26. Retrieved 2007-09-04.

Luxating patella cannot be present without the knee being loose, but a loose knee is not necessarily slipping out of the joint. Even with luxating patella, symptoms such as intermittent limping in the rear leg might be mild or absent. Physical examination and manual manipulation are the preferred methods for diagnosis. More extreme cases can result in severe lameness. [Osteoarthritis](https://en.wikipedia.org/wiki/Osteoarthritis "Osteoarthritis") typically develops secondarily.[^4b]

[^4b]: See ["Patellar Luxation"](https://web.archive.org/web/20070826115200/http://www.offa.org/patluxinfo.html). [Orthopedic Foundation for Animals](https://en.wikipedia.org/wiki/Orthopedic_Foundation_for_Animals "Orthopedic Foundation for Animals"). Archived from [the original](http://www.offa.org/patluxinfo.html) (text/html) on 2007-08-26. Retrieved 2007-09-04.

The four recognized diagnostic grades of patellar luxation include, in order of severity:[^4c]

[^4c]: See ["Patellar Luxation"](https://web.archive.org/web/20070826115200/http://www.offa.org/patluxinfo.html). [Orthopedic Foundation for Animals](https://en.wikipedia.org/wiki/Orthopedic_Foundation_for_Animals "Orthopedic Foundation for Animals"). Archived from [the original](http://www.offa.org/patluxinfo.html) (text/html) on 2007-08-26. Retrieved 2007-09-04.

### Grade I

-   Grade I - the patella can be manually luxated but is reduced (returns to the normal position) when released.

### Grade II

-   Grade II - the patella can be manually luxated or it can spontaneously luxate with [flexion](https://en.wikipedia.org/wiki/Flexion "Flexion") of the stifle joint. The patella remains luxated until it is manually reduced or when the animal extends the joint and derotates the [tibia](https://en.wikipedia.org/wiki/Tibia "Tibia") in the opposite direction of luxation.

### Grade III

-   Grade III - the patella remains luxated most of the time, but can be manually reduced with the stifle joint in extension. Flexion and extension of the stifle results again in luxation of the patella.

### Grade IV

-   Grade IV - the patella is permanently luxated and cannot be manually repositioned, with up to 90° of rotation of the proximal tibial plateau. The [femoral trochlear](https://en.wikipedia.org/wiki/Femur "Femur") groove is shallow or absent, with displacement of the [quadriceps](https://en.wikipedia.org/wiki/Quadriceps "Quadriceps") muscle group in the direction of luxation.

## Treatment

Grades II, III, and IV require [surgery](https://en.wikipedia.org/wiki/Surgery "Surgery") to correct, if the animal has difficulty walking. The surgery required is governed by the type of abnormality present, but often involves a [sulcoplasty](https://en.wikipedia.org/wiki/Sulcoplasty "Sulcoplasty"), a deepening of the trochlear sulcus where the patella sits, a realignment of the attachment of the patella tendon on the tibia, and tightening or releasing of the capsule on either side of the patella, according to which side the patella is slipping. Some grade IV conditions may require more involved surgery to realign the femur and/or tibia.

A therapeutic dosage of [glucosamine](https://en.wikipedia.org/wiki/Glucosamine "Glucosamine") can be used as a preliminary treatment to strengthen ligaments and the surrounding tissues of the joint and can delay or prevent surgery.[^5]

[^5]: See Ward, DVM, Ernest. ["Cruciate Ligament Rupture in Dogs"](http://vcahospitals.com/know-your-pet/luxating-patella-or-kneecap-in-dogs). _VCA_. Retrieved 2020-01-23.

Additional help can be given with the use of pet ramps, stairs, or steps. These can help the animal travel from one place to another, especially up and down, without adding any pain or damage to the patella.

## Discussion

Note to breeders: Keep in contact with the owners to ensure this is does not get passed onto further generations.
