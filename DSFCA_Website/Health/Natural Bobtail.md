---
source: https://vgl.ucdavis.edu/test/natural-bobtail
tags:
- Health
---

**Navigation**

- Parent:: [Health](Health.md)
- Peer:: 
- Child:: 

# Natural Bobtail

## Testing

[Natural Bobtail | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/natural-bobtail)

## Information

The material in this section is from: [Natural Bobtail | Veterinary Genetics Laboratory](https://vgl.ucdavis.edu/test/natural-bobtail)

### Phenotype

Dogs with natural bobtail have naturally bobbed (shortened) tails. Length of the bobbed tail is variable; some individuals may have nearly full-length tails while others may have virtually no tail.

### Mode of Inheritance

Autosomal dominant

### Alleles

**N** = Normal (no natural bobtail), **BT** = Natural bobtail

### Breeds appropriate for testing

Australian Cattle Dog, Australian Shepherd, Austrian Pinscher, Bourbonnais Pointer (Braque du Bourbonnais), Brazilian Terrier, Brittany Spaniel, Catahoula Leopard Dog, Croatian Sheepdog, Danish/Swedish Farmdog, Jack Russell Terrier, Karelian Bear Dog, McNab, Mudi, Pembroke Welsh Corgi, Polish Lowland Sheepdog, Pyrenean Shepherd, Savoy Sheepdog, Schipperke, Spanish Waterdog, Swedish Vallhund

### Explanation of Results

-   Dogs with **N/N** genotype do not have this natural bobtail variant. They will most likely have normal length tails, though other genetic factors may affect tail length. They cannot transmit this natural bobtail variant to their offspring.
-   Dogs with **N/BT** genotype are expected to have a natural bobtail. They may transmit this natural bobtail variant to 50% of their offspring. Matings between two N/BT dogs are expected to produce 50% puppies with natural bobtail, 25% puppies with normal length tails, and 25% puppies with the embryonic lethal BT/BT genotype (an expected 25% reduction in litter size).
-   Dogs with **BT/BT** genotype are expected to terminate development in utero (embryonic lethal); this genotype is not expected to be seen in live dogs. If a BT/BT genotype puppy is born alive, it may display severe developmental defects, including spinal deformities, that are incompatible with life.

[_Results of this test can be submitted to the OFA (Orthopedic Foundation for Animals)_](https://vgl.ucdavis.edu/test-results-ofa)

### Additional Details

Natural bobtail (NBT) is a naturally occurring mutation in the T-box transcription factor _T_ gene that results in a shortened tail. The mutation, a single nucleotide variant (c.189C>G), is inherited in an autosomal dominant fashion with both sexes equally affected. This mode of inheritance means that the presence of one copy of the variant will produce the bobbed tail phenotype in males and females. The length of the bobbed tail is variable and under the influence of other as-yet-undetermined genetic factors that cause some NBT individuals to have nearly full-length tails while others may have virtually no tail.

While heterozygous individuals (N/BT genotype, possessing one normal and one affected _T_ gene) have shortened tails, data suggest that the homozygous condition (BT/BT genotype, possessing two copies of the affected gene) is lethal in utero in most cases. Breeding of two heterozygotes (a N/BT x N/BT mating) is predicted to produce 25% N/N puppies with normal length tails, 50% N/BT puppies with natural bobtail, and 25% homozygous affected BT/BT offspring. Most BT/BT genotype offspring are predicted to terminate in utero and therefore represent a potential 25% reduction in litter size.

One report in the veterinary literature identified two tailless Pembroke Welsh Corgi puppies with BT/BT genotype that survived to term but displayed severe developmental abnormalities incompatible with life, including anorectal atresia and spinal defects. Both puppies were from the same parents but different litters, and neither survived long after birth. This small sample size and lack of further reports suggests that it is rare for BT/BT puppies to reach full term.

Genetic testing is recommended to verify and validate the natural bobtail status of dogs, especially if docking is allowed, and to help with breeding pair selection to decrease the risk of reduced litter size and lethal deformities. Testing for natural bobtail assists owners and breeders in identifying dogs that have this trait.

## Discussion
