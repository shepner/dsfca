---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](../Breed%20Standard.md)
- Peer:: 
- Child:: 

# Ear types

Any combination of the accepted ear types is permitted and can also occur just by how the ears are being held.  It should be commented upon by the judge:

| Type              | Accepted | [Occurrence](../../Occurrence.md) |
| ----------------- |:--------:| --------------------------- |
| Hanging           |   TRUE   | Frequent               |
| Button            |   TRUE   | Expected                    |
| Rose              |   TRUE   | Uncommon                    |
| Tipped            |          | Not expected                |
| Erect/tulip/prick |          | Not expected[^1]            |
| Silly             |   TRUE   | Expected[^2]                |

[^1]: This is *not* an accepted ear type *unless* upside down, from wind, looking up, etc.
[^2]: Routine daily occurrence.

## Hanging

![](_resources/Pasted%20image%2020230331203224.png)

Hanging years as shown above are at the limit of what would be permitted as the break is still, somewhat, above the skull. Fully hanging ears (like the Beagle shown below) would be a fault

![](_resources/Pasted%20image%2020230508173617.png)

## Button

Closer to the skull is preferred:

![](_resources/Pasted%20image%2020230331202327.png)

These are permitted but are getting close to being Tipped ears which would be a fault:

![](_resources/Pasted%20image%2020230331204851.png)

## Rose

![](_resources/Pasted%20image%2020230331203036.png)

![](_resources/Pasted%20image%2020230331204120.png)

![](_resources/Pasted%20image%2020230331204140.png)

## Tipped

This is a fault

pictures??

## Erect/tulip/prick

This is a fault if it is the normal ear position

![](_resources/Pasted%20image%2020230331202819.png)



![](_resources/Pasted%20image%2020230401081837.png)

![](_resources/Pasted%20image%2020230422081503.png)

## Silly

aka 'sporty', 'fun', and 'aerodynamic'.

![](_resources/Pasted%20image%2020230331203547.png)

![](_resources/Pasted%20image%2020230331205003.png)
