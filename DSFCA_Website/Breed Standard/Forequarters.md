---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](Breed%20Standard.md)
- Peer:: 
- Child:: 

# Forequarters

discussion and pictures of all sides showing items below for each gender

- Upper Arm
- Shoulder Blade
- Legs
- Pasterns
- Dewclaws
- Feet

Male (left) vs female (right):
![](_resources/Pasted%20image%2020230514182738.png)

Male:
![](_resources/Pasted%20image%2020230514182935.png)

Female:
![](_resources/Pasted%20image%2020230514183008.png)
