---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [README](../README.md)
- Peer:: [Breeding strategy](../Breeding%20strategy.md), [Work](../Work/Work.md), [Temperament](../Temperament/Temperament.md), 
- Child:: 

# Breed Standard

!!! TODO "it would be nice (better?) if each section below was its own document that is embedded into this one. Each containing addl information and pictures beyond the basic definition shown on this page"

!!! TODO "what about a judging 'cheat sheet'?"

## Introduction

The Danish-Swedish Farmdog is a cheerful, small, compact, smooth-coated, and multipurpose barnyard dog. It originated in Denmark and Southern Sweden and was commonly found on small farms up until the Industrial Revolution, when many of those farms closed down.  
  
While its specialty was vermin control, it could occasionally be found hunting or helping to bring the cows in. Hunting and herding were not originally among its main purposes and are, therefore, not required abilities. Its skill and drive to chase mice, rats and other vermin continues to be a characteristic of the breed. It is coordinated, agile, and blazingly fast - important abilities for a vermin catcher![^1]

[^1]: See: [Work](../Work/Work.md)
  
The Danish-Swedish Farmdog is a friendly, outgoing, attentive, and lively companion. Frequently a playmate to the farmer’s children, it is well suited as a companion. Its open minded, quick-to-learn style earned it a place in old-time circuses as a performer and now endears it to dog sport enthusiasts and families alike.[^2]

[^2]: See: [Temperament](../Temperament/Temperament.md)
  
The Farmdog served as an able little watchdog, alerting its owners to the presence of strangers approaching the farm. Its warning bark serves a strictly “doorbell” function. It must never show aggression, quieting as soon as the owner has accepted the visitor. It must be friendly, greeting visitors with a wagging tail. A reserved and shy Farmdog has a completely undesirable temperament.  
  
## General appearance

A small, compact, lively, smooth coated, and slightly rectangular dog. Known to mature late.[^3] The Danish-Swedish Farmdog is not to appear refined or elegant.[^4] Clear differentiation between the genders is important.

[^3]: See: [Aging](Aging/Aging.md)
[^4]: See: [Faults](Faults.md)

### Size, proportion, and substance

#### Height at the withers

- Males: 13.5 - 14.5 inches +/- 1 inch.
- Females: 12.5 - 14 inches +/- ½ inch.

#### Proportion

The Farmdog should be slightly longer than tall. It must not give the impression of either long or short legs. The body should be slightly rectangular, a proportion of 9:10. The proportions between depth of chest and height at withers should be 1:2.  (See [Aging](Aging/Aging.md)) (See [Neck, body, and topline](Neck,%20body,%20and%20topline.md))

#### Substance

Farmdogs develop slowly. This should be considered during judging. The breed is not fully developed until the age of 3-4 years, in some cases even longer for males. (See [Aging](Aging/Aging.md))

**[Faults](Faults.md):** Elegant general appearance.

#### [Head](Head.md)

The head should be triangular and a bit small in proportion to the body. The rather broad skull creates the basis and the head is gradually narrowing towards the muzzle, which is slightly shorter than the skull.  
  
##### Skull

Rather broad and slightly rounded.

##### Stop

Well defined. Cheeks pronounced without exaggeration.

##### Eyes

Medium-sized, slightly rounded, neither protruding nor sunken. Attentive and kind expression. Dark eye color in dogs with black patches. Slightly lighter eye color permissible in dogs with yellow, or liver/brown patches.  

##### [Ears](Ears/Ears.md)

Medium-sized. Rose or button, in both cases the fold should be just above the skull. It is not unusual to see a Farmdog with one of each. Button ears: The tips should lie close to cheeks. 

**[Faults](Faults.md):** Prick ears.

##### Muzzle

Well-developed and gradually narrowing towards the nose, but must not give a snipey impression. Muzzle slightly shorter than the skull. Nose bridge straight.  

##### Jaws

Strong.

##### Nose

Color in accordance with the color of patches.

(See: [Color and markings](Color%20and%20markings/Color%20and%20markings.md))

##### Bite

Scissors bite with even and well-developed incisors. A level bite is acceptable.

#### [Neck, body, and topline](Neck,%20body,%20and%20topline.md)

##### Neck

Medium length, strong and slightly arched. No throatiness.

**Fault:** Neck too long.  (See [Faults](Faults.md))

##### Body

Compact with good substance. The body is substantial due to the deep and roomy rib cage, which has a good spring of ribs. A fully developed, compact body cannot be expected until maturity, but it is important that the proportions are correct even in young dogs.  

##### Topline

Strong back, slightly arched short loin, slightly rounded croup. If the tail is set too high, the croup is too flat and this gives the impression of terrier-type.

##### Loin

Short, broad, and slightly arched.  

##### Croup

Slightly rounded. Must not be flat. The slightly rounded croup must never be confused with a steep or sloping croup. An overly sloping croup does not facilitate the breed’s free movement.

**[Faults](Faults.md):** Steep croup.

##### Chest

Long, deep, and roomy with very well sprung ribs. Fore chest well defined. The front is slightly broader than the ribcage. The chest should reach to the elbow.

**[Faults](Faults.md)**: Lack of depth in the chest. Narrow in front.

##### Ribs

Well sprung. The ribcage must be relatively wide compared to the size of the dog.

**[Faults](Faults.md):** Flat or short rib cage.

##### Underline/tuck up

Belly only slightly tucked up. The long rib cage and short loin, make for an only slightly tucked up belly.  

##### Tail

Not too high tail set. Long tail or naturally bobtailed. It is never docked. The tail should be carried straight with a slight curve or like a sickle. The slightly rounded croup causes the tail not to be set too high.

**[Faults](Faults.md):** Curled tail or tail carried flat on back.

#### [Forequarters](Forequarters.md)

##### Upper Arm

Oblique, not too steep.  

##### Shoulder Blade

Oblique, not too steep.

##### Legs

Front legs straight and parallel. Due to the wide and deep chest, the front legs are placed rather wide.

**[Faults](Faults.md):** Low on legs.

##### Pasterns

Strong and springy.  

##### Dewclaws

Front dewclaws are present.  

##### Feet

Small, oval, and moderately tight-knit.   

#### Hindquarters

##### Legs

Parallel and well-muscled.  

##### Thigh

Fairly broad. Stifle: Well angulated.  

##### Hock Joint

Well angulated.

##### Rear Dewclaws

Accepted  

#### Coat

The coat is short, hard, and smooth.

#### [Color and markings](Color%20and%20markings/Color%20and%20markings.md)

White dominating. Patches of different colors, sizes, and combinations permissible, (black, brown, agouti, yellow, and all shades of tan and fawn). Patches also on head, with or without tan markings. Flecking accepted.

**[Faults](Faults.md):** Head entirely white.

#### [Gait](Gait.md)

Parallel and free  

### [Temperament](../Temperament/Temperament.md)

Alert, friendly, attentive, and lively.  

### [Faults](Faults.md)

Any departure from the foregoing points would be considered a fault and the seriousness with which the fault should be regarded should be in proportion to its degree and effect on the health and welfare of the dog.  
  
### Conformation Judging Procedure  
  
The Danish-Swedish Farmdog is to be examined on the table.  


## Addl material

This little group of selected dogs, were "browsing around" at the international network meeting in Denmark yesterday. Their purpose was to show all of the representatives from the other countries, what breeding goal should be, if wanting to breed true to the Danish and Swedish correct type. These 5 were hand picked by the Danish and Swedish club's working group for this purpose, representing as near correct interpretation of breed standard as can be.

They are a male (left) with no exaggerations, yet clearly masculine, then 4 clearly feminine females: A female at 7 years old (works every day as a ratter for a professional exterminator), a female 2 ½ years old, another 7 year old female, and a 5 year old. These dogs have topped the podiums over and over before numerous different judges in Denmark and Sweden, and they have titled at major farmdog shows of 50-100+ dogs.

They are in accordance with the breed standard "small and compact" and there is nothing elegant about them, yet they are agile and fast. Friendly and eagor. They do not live together, but come from 3 different breeders and 4 different households.

This is the "mold" for the breeding goal of type and proportions, in accordance with Denmark and Sweden.

![](_resources/Pasted%20image%2020230413105156.png)

the dogs are left to right: Zkrubbe's Heartbreaker, GuddommeligeGry, Little Denmark's U-Name-It UglyDuckling, Little Denmark's Maya My Eenie and Little Denmark's Ratpack Rosie B. They are thereby from 3 different breeders.

The point of this group is actually that there is no variety in type and size here other than the younger dog being slightly narrow'er than the adult dogs with several litters behind them. They all 5 fit straight and slick into the exact same mold.

## Other breed standards

FCI: [DANSK-SVENSK GÅRDSHUND](https://www.fci.be/en/nomenclature/DANISH-SWEDISH-FARMDOG-356.html)


