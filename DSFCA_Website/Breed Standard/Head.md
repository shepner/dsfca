---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](Breed%20Standard.md)
- Peer:: 
- Child:: 

# Head

discussion and male and female pictures.  notations on the various features below called out in the standard and indicating the range of 'acceptable'

- Skull
- Stop
- Eyes
- Muzzle
- Jaws
- Bite

Male (left) vs female (right):
![](_resources/Pasted%20image%2020230514182738.png)

Male:
![](_resources/Pasted%20image%2020230514182935.png)

Female:
![](_resources/Pasted%20image%2020230514183008.png)
