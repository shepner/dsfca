---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](../Breed%20Standard.md)
- Peer:: 
- Child:: 

# Color and markings

There is a pretty wide range of color and markings.  Provide examples of them

anything about frequency of color types?
| type  | [Occurrence](../../Occurrence.md) |
| ----- | ------------------------------ |
| Plaid | Frequent[^1]                   |

[^1]: Mostly sighted during winter in Northern climates.

## Plaid

![](_resources/Pasted%20image%2020230401094519.png)


## Color by age

The black markings on the head generally will turn brown on their heads as they age:

5 weeks old
![5 weeks old](_resources/Pasted%20image%2020230415073149.png)

![](_resources/Pasted%20image%2020230415073202.png)

![](_resources/Pasted%20image%2020230415073212.png)

![](_resources/Pasted%20image%2020230415073222.png)

![](_resources/Pasted%20image%2020230415073234.png)

![](_resources/Pasted%20image%2020230415073245.png)

6 months old
![6 months old](_resources/Pasted%20image%2020230415073257.png)

The black headband shown above will likely fade away after a year.

While it seems this is less common, here are some adult examples of where the black did not fade:

![](_resources/Pasted%20image%2020230418171853.png)

![](_resources/Pasted%20image%2020230418172038.png)


More:

![](_resources/Pasted%20image%2020230514182935.png)

![](_resources/Pasted%20image%2020230514183008.png)

