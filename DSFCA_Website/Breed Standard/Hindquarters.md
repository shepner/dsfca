---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](Breed%20Standard.md)
- Peer:: 
- Child:: 

# Hindquarters

discussion and pictures of all sides showing items below for each gender

- Legs
- Thigh
- Hock Joint
- Rear Dewclaws

Male:
![](_resources/Pasted%20image%2020230514182935.png)

Female:
![](_resources/Pasted%20image%2020230514183008.png)

![](_resources/Pasted%20image%2020230521073446.png)