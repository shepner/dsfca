---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](Breed%20Standard.md)
- Peer:: 
- Child:: 

# Faults

discussion and pictures of the faults.  point out the more common ones to watch for

## Elegant general appearance

what exactly does this mean?

## Prick ears

See [Ears](Ears/Ears.md)

## Neck too long

## Steep croup

## Lack of depth in the chest, narrow in front

## Flat or short rib cage

## Curled tail or tail carried flat on back

## Low on legs

Is this saying 'short'?

