---
source: !
tags:
- Breed_standard
---

**Navigation**

- Parent:: [Breed Standard](Breed%20Standard.md)
- Peer:: 
- Child:: 

# Neck, body, and topline

pictures of all sides showing items below for each gender

- Neck
- Body
- Topline
- Loin
- Croup
- Chest
- Ribs
- Underline/tuck up
- Tail
- Curled tail or tail carried flat on back

7 month old male who's rear legs are longer than the front.  He may still grow out of this:
![](_resources/Pasted%20image%2020230510162204.png)

Male (left) vs female (right):
![](_resources/Pasted%20image%2020230514182738.png)

Male:
![](_resources/Pasted%20image%2020230514182935.png)

Female:
![](_resources/Pasted%20image%2020230514183008.png)

![](_resources/Pasted%20image%2020230521073422.png)