Source: [rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/aktiviteter/f%C3%A4rggenetik?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)


On 25/11 2021, a webinar from Rasklubben was shown where Hilde Nybom talked about Genetics and color inheritance of DSG. Here is a small selection of what was covered in the lecture.

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/55/a1/55a1967e-fd06-4508-9b2c-47ad80f8f6f1.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/55/a1/55a1967e-fd06-4508-9b2c-47ad80f8f6f1.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/5f/92/5f926d7d-ceba-4ef0-a41c-c16b58c1b313.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/5f/92/5f926d7d-ceba-4ef0-a41c-c16b58c1b313.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d5/96/d5966fda-13ca-48a0-8806-c72586fb8cab.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d5/96/d5966fda-13ca-48a0-8806-c72586fb8cab.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/3d/3a/3d3aa145-d7cd-4368-b4cf-b180a965c8f5.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/3d/3a/3d3aa145-d7cd-4368-b4cf-b180a965c8f5.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/62/2d/622d7ba3-c19d-46f4-ab10-79542d234c5a.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/62/2d/622d7ba3-c19d-46f4-ab10-79542d234c5a.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/79/c9/79c91ad6-2d2c-4c36-bcca-f2be3b91957a.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/79/c9/79c91ad6-2d2c-4c36-bcca-f2be3b91957a.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/bf/85/bf85a2f2-f3e8-4036-aed5-1f6dbbcd5be9.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/bf/85/bf85a2f2-f3e8-4036-aed5-1f6dbbcd5be9.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d2/06/d20631df-9ee5-4574-8b2c-6c868daa09a7.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d2/06/d20631df-9ee5-4574-8b2c-6c868daa09a7.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/15/81/1581ed2b-fae6-41c7-93a8-3ddee1aebbca.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/15/81/1581ed2b-fae6-41c7-93a8-3ddee1aebbca.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b4/b3/b4b3e7d4-2f59-4fe3-9d21-f2b9bfb76eb0.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b4/b3/b4b3e7d4-2f59-4fe3-9d21-f2b9bfb76eb0.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/63/46/63466d96-a2f4-437e-853c-ab26ef438742.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/63/46/63466d96-a2f4-437e-853c-ab26ef438742.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/e4/29/e429077b-c437-482a-abb2-e91a66854a5f.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/e4/29/e429077b-c437-482a-abb2-e91a66854a5f.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/76/b3/76b36bfa-ae75-4b99-b212-46805455078b.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/76/b3/76b36bfa-ae75-4b99-b212-46805455078b.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/bf/ad/bfad2867-c486-4cdc-8b30-2225ef3f461d.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/bf/ad/bfad2867-c486-4cdc-8b30-2225ef3f461d.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b1/e2/b1e206b8-2dbf-4f0a-936e-2a19078739bd.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b1/e2/b1e206b8-2dbf-4f0a-936e-2a19078739bd.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/56/46/56465d8a-3fec-45b2-928d-29a14ddc98d2.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/56/46/56465d8a-3fec-45b2-928d-29a14ddc98d2.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d2/f2/d2f2e4a9-75b8-453e-a48c-d4ecea3da792.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d2/f2/d2f2e4a9-75b8-453e-a48c-d4ecea3da792.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/1d/7a/1d7a60ec-c2f9-4d17-b92f-d68bc32074d9.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/1d/7a/1d7a60ec-c2f9-4d17-b92f-d68bc32074d9.JPG)

18

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c7/8d/c78de6f8-b389-459f-85d2-70c0dfe94765.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c7/8d/c78de6f8-b389-459f-85d2-70c0dfe94765.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/e3/db/e3db1bd5-950f-4f8a-b099-f02367d2d4e4.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/e3/db/e3db1bd5-950f-4f8a-b099-f02367d2d4e4.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c6/11/c6110494-8ae3-4fce-8300-23f6ebb9d63c.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c6/11/c6110494-8ae3-4fce-8300-23f6ebb9d63c.JPG)

21

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/ad/94/ad94e1fb-97a9-4f82-a13e-c4583776ef3f.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/ad/94/ad94e1fb-97a9-4f82-a13e-c4583776ef3f.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/35/fa/35fa400e-3608-4ceb-9d0a-b492c8b308bd.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/35/fa/35fa400e-3608-4ceb-9d0a-b492c8b308bd.JPG)

23

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/f0/97/f0972864-40bf-4bce-9ac9-489dc761f6db.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/f0/97/f0972864-40bf-4bce-9ac9-489dc761f6db.JPG)

24

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/a8/12/a812d3bd-37e2-4491-a8ac-c17407f4f549.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/a8/12/a812d3bd-37e2-4491-a8ac-c17407f4f549.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/81/3b/813b8504-caeb-4871-a63d-41ff2c03126a.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/81/3b/813b8504-caeb-4871-a63d-41ff2c03126a.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fb/60/fb608488-a957-408c-a600-f6b0b7bb33c0.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fb/60/fb608488-a957-408c-a600-f6b0b7bb33c0.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/bc/13/bc13a207-409f-47b7-8f05-a42edaa4f00c.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/bc/13/bc13a207-409f-47b7-8f05-a42edaa4f00c.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/42/e8/42e8883c-e508-411f-a845-21aacaaaaac9.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/42/e8/42e8883c-e508-411f-a845-21aacaaaaac9.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/5d/5a/5d5ac3ea-92f3-4394-af2c-1dadb73e9e04.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/5d/5a/5d5ac3ea-92f3-4394-af2c-1dadb73e9e04.JPG)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b1/38/b1388357-ecdd-4611-ac00-bf7852411467.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b1/38/b1388357-ecdd-4611-ac00-bf7852411467.JPG)
