
Source: [BPH &amp; MH | gardshund.com](https://gardshund-com.translate.goog/aktiviteter/bph-mh-35478584?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)



---

## BPH - BEHAVIOR AND PERSONALITY DESCRIPTION DOG

BPH is a description – not an exam or a test. This means that a dog is not evaluated as approved or not approved, which is important to know. In SKK's Breeding Data it is noted that the description has been carried out and the result is reported in a spider diagram which makes it possible to compare an individual with other described dogs in the breed.  
Read more about BPH and register at [www.skk.se/bph](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.skk.se/bph) .  
  
[Analysis profile of the farm dog](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/globalassets/dokument/bph-analys/200-analys-bph-dansk-svensk-gardshund.pdf) .

## MH - MENTAL DESCRIPTION DOG

Mental description Dog is one of the world's most used tests for dogs. It consists of ten standardized situations where a number of sub-moments are graded on an intensity scale. The purpose is to describe the dog's social ability, fear, aggressiveness and playfulness, etc. You want to see how the individual dog and/or the breed in general handles, for example, surprising sounds, threatening objects that slowly approach or is able to cooperate with strangers.

---

**Mentally describe your dog!**

 **Find out more about your dog's personality**

Learning more about your dog's personality can be both beneficial and enjoyable. As a dog owner, you can get an overall picture of how your dog reacts in different situations. For breeders, the description provides important information for the breeding work. BPH, or behavioral and personality description dog, is a mental description that is adapted for all dog breeds. During the description, which takes about 30 minutes, the dog gets to meet strangers, play, work to get food and encounter surprises, among other things.

**For more information for more info and watch the BPH film:  [www.skk.se/bph](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.skk.se/bph)** 

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/dd/3e/dd3eda26-e9b3-4ff5-a68c-26e6c293c313.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/sv/uppfodning/mentalitet/beteende-och-personlighetsbeskrivning-hund-bph/?_t_id%3D1B2M2Y8AsgTpgAmY7PhCfg%253d%253d%26_t_q%3Dbph%26_t_tags%3Dandquerymatch%252clanguage%253asv%257clanguage%253a7D2DA0A9FC754533B091FA6886A51C0D%252csiteid%253a821afd82-44fd-4b1f-8345-260bc2345d00%257csiteid%253a84BFAF5C52A349A0BC61A9FFB6983A66%252clanguage%253asv%26_t_ip%3D98.128.196.181%26_t_hit.id%3DSkk_Skk_se_Business_Models_Pages_StandardPage/_0c054ea0-4798-43f0-9266-364ae1a09ecf_sv%26_t_hit.pos%3D1)

# 

-   Behavior and personality description dog – BPH  
      
    
-   Do you want to know more about your dog's mentality?  
      
    The Swedish Kennel Club's mental description BPH is designed to suit all dogs. It helps you as a dog owner to get to know your dog a little better and is valuable in breeding work for breeders, breed and specialty clubs.

  

#####   

##### BPH 200 analysis and 500 lecture, the difference between these two

[BPH – 200-analys | Svenska Kennelklubben](https://www.skk.se/sv/uppfodning/mentalitet/beteende-och-personlighetsbeskrivning-hund-bph/For-uppfodare-och-klubbar/bph-analyser/?fbclid=IwAR0_CuG96JkQ886COoWUaJAGJCGRLkLf4DAlvGotTYwkL3pK_ppXjk2p9og)

**[COMPILATION OF BPH RESULTS FOR DANISH-SWEDISH FARM DOG](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/globalassets/dokument/om-skk/bph/bph-analys/200-analys-bph-dansk-svensk-gardshund.pdf)**

---

## BPH & MH ROSETTES

The club sponsors members' personally designed bows with the club's logo on the button.  
  
As a breeder, you may want to encourage your puppy buyers to participate in a BPH/MH description and, after the description has been completed, give your puppy buyers a bow as proof of appreciation. Or if, as a dog owner, in addition to the protocol, you want a nice "proof" of completed BPH/MH description. .  
  
If you want to order a bow, contact [**irene.scholander@hotmail.com**](mailto:irene.scholander@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)[  
](mailto:irene@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)[  
](mailto:irene@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/64/54/64547e9a-c975-4d51-8483-58e19b220ca3.jpg "COMPLETED BPH")](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/64/54/64547e9a-c975-4d51-8483-58e19b220ca3.jpg "COMPLETED BPH")

COMPLETED BPH

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8b/cd/8bcded38-8331-4711-9fad-02f4586878b4.jpg "CARRIED OUT MH")](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8b/cd/8bcded38-8331-4711-9fad-02f4586878b4.jpg "CARRIED OUT MH")

CARRIED OUT MH
