
Source: [Om gårdshunden | gardshund.com](https://gardshund-com.translate.goog/g%C3%A5rdshund/om-g%C3%A5rdshunden-35478461?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

# **THE FARM DOG**

  
[The Open Race](http://files.builder.misssite.com/1d/92/1d92532e-5f51-49c4-b7b0-c3eff03f7ab9.pdf) (link to the breed standard PDF)
  

## HISTORY

The breed was recognized in 1987 in Sweden and Denmark under the name Danish-Swedish farm dog. It has long existed on farms in Denmark and Sweden. The farm dog has been used as a guard dog, rat catcher and companion.  
The cynological origin of the Danish-Swedish farm dog is unclear. It probably has its main origins partly in the pinscher breeds and partly in the British white hunting terriers, from which the Jack Russell terrier and the Fox terrier also originate.

##   
AREAS OF USE

The farm dog is a small, lively, healthy, versatile family dog ​​that easily adapts to most situations. The dog sports agility, freestyle, track, obedience, work and exhibition are well suited as activities. Many also train and compete in game trails.  
Even today, some Danish-Swedish farm dogs serve as rat hunters, but the vast majority are companion dogs. An estimated dozen or so dogs are used as active hunting dogs for short-term driving of deer and other smaller game, retrieving and searching.

  

## HEALTH  
The farm dog is generally a healthy breed. 

  

## CHARACTERISTICS AND MENTALITY  

The farm dog likes to work both physically and mentally, and it requires some activation. Although the breed is small in size and looks cute, it is therefore a rather demanding dog.  
It needs some form of regular stimulation to feel good.  
Between work shifts, it is a real cuddly pig that likes to crawl up on your lap, lie close to you and relax. The farm dog likes warmth a lot and often lies down and relaxes in the sun.  
It is a distinct watchdog that immediately sounds the alarm if strangers enter its home area. Otherwise, it is not a reasonable breed.

## SIZE AND APPEARANCE

Wither height for males is approx. 34–37 cm and bitches is approx. 32–35 cm. It has a lot of body mass. The body is slightly rectangular. The head is small in relation to the body.  
The tail is naturally long, semi-long or the dog can be born with a stubby tail. The fur color is predominantly white with one or more spots in different color combinations.

## FUR CARE

Fur care is basically non-existent on the breed because the fur must be hard, short and smooth without under wool. During the shedding period, you can brush the dog through daily, which is usually enough to keep the fur nice, and in addition, you can give it a bath if necessary.

## OTHER

The breed is part of Swedish cultural heritage. The breed club works actively to preserve the breed as a healthy, alert, capable, friendly and accessible farm and companion dog with the long-documented, breed-typical exterior.
