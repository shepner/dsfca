Source: [Länkar | gardshund.com](https://gardshund-com.translate.goog/g%C3%A5rdshund/l%C3%A4nkar-36000259?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

## LINKS

Here we have collected some links for those who want to learn more about the farm dog, train or compete in some dog sport.

## CLUBS

[DKK Hundeweb](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.hundeweb.dk/dkk/public/openIndex?ARTICLE_ID%3D1)  - Danish Kennel Club  
[Kennelliitto.fi](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.kennelliitto.fi/sv)  - Finnish Kennel Club  
[NKK Dogweb](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.dogweb.no/dogweb/dw/openPage/hoved.html)  - Norwegian Kennel Club  
[RDSG Denmark](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dsgk.dk/)  - Breed Club in Denmark  
[RDSG Finland](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://pihakoirat.net/)  - Breed Club in Finland  
[RDSG Norway](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://rdsg.no/)  - Breed Club in Norway  
[RDSG USA](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.farmdogs.org/)  - Breed Club in the USA  
[SGVK](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://sgvk.se/)  - Swedish Farm and Herding Club  
[SKK](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/)  - Swedish kennelklubben  
[Jordbruksverket](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.jordbruksverket.se/)  - General information about animal welfare  
[SKK breeding data](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://hundar.skk.se/avelsdata/Initial.aspx)  - Database for breeding registers  
[SKK Hunddata](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://hundar.skk.se/hunddata/)  - Database of registered dogs  
[AGIDA](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://agilitydata.se/logga-in/) - Agility data  
[SAGIK](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://agilityklubben.se/) -  Swedish Agility Club  
[SHFK](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://shfk.se/)  - Swedish Dog Freestyle Club [RASDATA.nu DSG](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.rasdata.nu/gardshund/)  
  
  
  

## ACTIVITIES

[SKK Exhibition](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://hundar.skk.se/internetanmalan/Start.aspx)  - For internet registration exhibition  
[SAGIK competition](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://agilitydata.se/logga-in/) - Registration for agility competition  
[SBK competition](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://sbktavling.se/)  - Registration for rally obedience, obedience and handling.
