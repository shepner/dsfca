
Source: [rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/avel/hanhundslista?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

## MALE DOG LIST

**Male dogs that meet the breed club's [breeding recommendations](https://gardshund-com.translate.goog/avel/avelsrekommendationer-35478600?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) are presented here .**  
  
Each individual male dog owner is responsible for ensuring that the information shown here is correct and up-to-date.  
  
It is the responsibility of both the female owner and the male dog owner to follow the rules and recommendations according to the SKK, the breed club and the Swedish Agency for Agriculture. The breed club also has recommendations that must be followed in order for the puppies to be advertised on the club's website.  
On the list of male dogs, there are males with different merits and characteristics.   
The breed club recommends checking all information in SKK's dog data and [breeding data](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://hundar.skk.se/avelsdata/Initial.aspx) before mating .  
There is information to download about e.g. BPH, show results, etc. Feel free to do a test mating via SKK's breeding data  
Remember to preserve the genetic diversity of the breed when mating combinations are planned and strive for a low inbreeding rate.  
Before mating, both male and female dog owners are obliged to inform each other of the presence of known hereditary diseases in the lines.  
Contact the male dog owner with your request well in advance of the planned mating.  
The male dog owner has every right to decline the mating request.  
  
**Update and deletion on the list!**  
If you, as a male dog owner, want to update your male dog's information, or if you want to remove him from the list,   
  
contact [webmaster@gardshund.com](mailto:webmaster@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) _**Mark the email "male dog list"**_ _F_ _forms can be filled in under "Male dog for breeding" when you want your male dog on the list_  

  
If you have a male dog in the list, be sure to update the number of offspring.  
  
**_A single male or female dog must not leave more than 50 offspring.  
  
The male dog is removed from the list when the number of offspring  
exceeds 50 offspring._**

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8c/72/8c728f69-af90-47ea-b1d8-d203cd3f3f41.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8c/72/8c728f69-af90-47ea-b1d8-d203cd3f3f41.jpg)

## Aagerup's Otto  

Helena Svedberg Nikula  
[helena_svedberg@hotmail.com](mailto:helena_svedberg@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
073 539 09 78  
Glemmingebro  
  

**Dog:**

Aagerups Otto SE15135/2013

**Exhibition:**

BOB

**Championships/Titles:**

  

**HD results:**

B

**Mentally described:**

BPH

**Other:**

  

**Number of offspring:**

0

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6f/3e/6f3e4ab7-75e3-4459-b9a5-4060d141b15c.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6f/3e/6f3e4ab7-75e3-4459-b9a5-4060d141b15c.jpg)

## Aagerup's Qrut  

Helena Svedberg Nikula  
[helena_svedberg@hotmail.com](mailto:helena_svedberg@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
073 539 09 78  
Glemmingebro  
  

**Dog:**

Aagerup's Qrut SE50881/2013

**Exhibition:**

Excellent

**Championships/Titles:**

  

**HD results:**

B

**Mentally described:**

BPH

**Other:**

  

**Number of offspring:**

6

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b0/ef/b0efa7e6-57c8-4128-aec9-f09bb3367a19.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/b0/ef/b0efa7e6-57c8-4128-aec9-f09bb3367a19.jpg)

## Aagerup's Äddy

Claes Rörstam  
rorstam@live.se  
Trelleborg  
070-98 91 401  
  

**Dog:**

Aagerup's Äddy  
SE52038/2018

**Exhibition:**

Excellent

**Championships/Titles:**

  

**HD results:**

  
A  
  

**Mentally described:**

BPH

**Other:**

  

**Number of offspring: 0**

 

  

---

**HAAKON OF ADORADO**

Isabell Melo [bellamac@yahoo.com](mailto:bellamac@yahoo.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) Tel: 073-443 90 80 Åsa, Halland  
[](mailto:bellamac@yahoo.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
  
  
  

**Dog:** 

Adorado's Haakon (Winston)SE39848/2017

**Exhibition:  
**BOB, CERT, CACIB

**Championships/Titles:  
  
**

**HD Score:  
**A**  
  
HD Index:  
**111 (4/7-22)      

**PLL:  
**Hereditary free 

**Mentally described:  
**BPH

**Other:  
**

Winston is a kind family dog ​​living on  
the west coast. He is a curious, energetic and happy guy.  
Trains and competes a bit in various dog sports.  
In addition to exhibition, it will mainly be Nosework  
as he loves to work with the nose, but also  
a little Rally Obedience and Agility.  
  
  
Scissor bite, fully toothed

**Number of offspring:**

11  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/25/37/2537990d-6638-4811-b25f-056c74a57dbb.jpeg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/25/37/2537990d-6638-4811-b25f-056c74a57dbb.jpeg)

---

**THE BOLD STORM OF THE ASPHARD**

Siv Åkerblom  
siv.m.akerblom@gmail.com  
072-200 04 44  
Leksand  
  

**Male dog: Aspgärdet's bold Storm  
  
Registration no: SE54127/2019**  

**Exhibition: VG  
  
**

**Championship/titles: SEVCH  
  
**

**HD results: A**

**HD index: 112    (2023-01-23 )**     

 **PLL: Hereditary free**

**Mentally described:**  

**Other: Molle is a safe, kind, lively, alert, happy, social, curious and child-loving dog who loves being with the family and being able to work. Molle is easy to learn, good at various tricks and arts and is very good at tracking game. Molle has HD A (A+A), ED ua (0) (0 + 0), patella ua, and scissor bite and is hereditary free as both parents are tested DNA clear. HD index: 113 Tri-coloured, 37 cm high and weighs approx. 10 kg. Molle is in Leksand in Dalarna.**                     

**Number of offspring: 0**  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/74/69/74697b23-5b6e-41d0-ae17-48da1e6cbf49.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/74/69/74697b23-5b6e-41d0-ae17-48da1e6cbf49.jpg)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fd/df/fddfb25c-39b3-4afe-9e23-3c0a6bc72049.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fd/df/fddfb25c-39b3-4afe-9e23-3c0a6bc72049.jpg)

## Attack by the Fenriswolf  

Isabelle Crusoe  
[icrusoe@live.se](mailto:icrusoe@live.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 5506490  
Västerås  
  

**Dog:**

Attack by the Fenriswolf SE42880/2013

**Exhibition:**

  

**Championships/Titles:**

SEE UCH

**HD results:**

B

**Mentally described:**

BPH

**Other:**

ED et al  
Patella et al

**Number of offspring**

28

  

---

### Björkils Bertsson   
  

  
  
[Heidrun Wilke-Pohnert](mailto:wilke-pohnert@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
Herbstein, Hessen  
Tel: +491718153599  
  
  

> **Male dog:**          Björkils Bertsson _SE37832/2018_ **Exhibition:**         Excellent ÖKL  
>                
> **Championship/titles:**       
> **HD result:**         B **Mentally described:** **BPH:** **PLL:** **hereditary PLL-free** **Other:**          

             A lovely male, lives in Germany but spends a few weeks in Sweden every year in Svanskog in Värmland

>   
>  **Number of offspring:**      0

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/55/bf/55bfb3b1-96c6-49a8-9604-753e0a012199.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/55/bf/55bfb3b1-96c6-49a8-9604-753e0a012199.jpg)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6c/9e/6c9e091c-98d0-4840-b820-9228b86fcdee.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6c/9e/6c9e091c-98d0-4840-b820-9228b86fcdee.jpg)

## Björkil's Wise Rålf   

Charlotte Egman  
[ce@etavo.com](mailto:ce@etavo.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
0708 622121  
Hägersten  
  

**Dog:**

Björkil's Rådige Rålf  
SE56147/2016

**Exhibition:**

Excellent

**Championships/Titles:**

  

**HD results:  
PLL:**  

  
A  
Hereditary free  
  

**Mentally described:**

  

**Other:**

Pleasant, social male who likes people and other dogs. Likes to chase rats.

He is good to have loose on the farm with chickens and geese.

**Number of offspring:**

16

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/86/ca/86cab7ed-c975-4042-926e-61fd2bfef240.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/86/ca/86cab7ed-c975-4042-926e-61fd2bfef240.jpg)

## Chico  

Ingela Hellberg  
[ingelanorgren@me.com](mailto:ingelanorgren@me.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 424 25 99  
Stockholm  
  

**Dog:**

Chico SE36478/2015

**Exhibition:**

Excellent

**Championships/Titles:**

  

**HD results:**

B

**Mentally described:**

BPH

**Other:**

Chico (called Toste) is a social and kind dog who loves people, especially children. He is safe and stable, unafraid of thunder, fireworks etc. Available in Stockholm and possibly Halland.

**Number of offspring:**

8

  

---

**DOBGUN'S LORD LANDON**

[Christina Strandberg  
Enköping](mailto:dobguns@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
+46760195798  
[http://www.dobguns.se](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dobguns.se)

  
**Male dog:**         Dobgun's Lord Landon SE42090/2018

  
**Exhibition:**         CACIB  
  
**Championship/titles:**     Slovenian, Croatian, Estonian,  
           Bulgarian Champion

**HD results:**       A

**Mentally described:**      BPH 

**Other:**        

**Avg** **mother:**      0

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fe/b7/feb7d511-f27a-47c1-b8ca-2455216e312f.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fe/b7/feb7d511-f27a-47c1-b8ca-2455216e312f.jpg)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/48/99/48999299-5438-456e-8d52-05fd64c26a0e.png)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/48/99/48999299-5438-456e-8d52-05fd64c26a0e.png)

## Dreamwind's Fantastic Tucker Jr  

Birger Wandt  
[birger.wandt@gmail.com](mailto:birger.wandt@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
073 918 52 53  
Gothenburg  
  

**Dog:**

Drömvinden's Fantastic Tucker Jr SE47436/2014

**Exhibition:**

BIM at the breed special in Vårgårda 2017, Cert, HP puppy, BIR junior, CK, Excellent

**Championships/Titles:**

  

**HD results:**

A  
  

**Mentally described:** 

  

**Other:**

  

**Number of offspring:**

0

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d8/6f/d86f89ea-d452-42fd-a72f-fc06fd9a6827.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d8/6f/d86f89ea-d452-42fd-a72f-fc06fd9a6827.jpg)

## Ellinghedehus Gorm  

Ingrid Wiklund  
[ingrid-wiklund@telia.com](mailto:ingrid-wiklund@telia.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 216 94 14  
Korskrogen  
  

**Dog:**

Ellinghedehus Gorm  
SE10676/2014

**Exhibition:**

Junior winner, Swedish show champion

**Championships/Titles:**

SEE UCH

**HD results:**

B

**Mentally described:**

BPH

**Other:**

  

**Number of offspring:**

4

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/f4/c2/f4c22f4b-e2f7-4ca4-800c-27c340aa0db9.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/f4/c2/f4c22f4b-e2f7-4ca4-800c-27c340aa0db9.JPG)

## ESKERYDS HUGO

Veronika Andersson

pokkadis@yahoo.se  

Äskeryd north 206

31495 KINNARED

070 296-6613

  

**Dog:**

Eskeryd's Hugo  
SE55492/2020

**Exhibition:**

Excellent CK  

**Championships/Titles:**

  

**Health Outcomes:**

HD: B, PLL Clear

**Other:**

Hugo is a very social and nice family dog.  
Lives with Fodervärd in Östersund.    
But may be available in Halland periodically  

**Number of offspring:**

5

  

---

Grazatis Jättecoola-Julle  
Lena Levenius        m_lev5825@hotmail.com Tel: 070-548 00 17 Kågeröd  
  
  
****Male dog:**      **GRAZATI'S GIANT Cola CHRISTMAS****    **Registration no:**     **SE34730/2020****

**Exhibition: Excellent** 

**Championships/  
titles:** 

**HD results: A**          

 **PLL: Hereditary free** 

**Mental description:          
BPH/MH** 

**Other: Works just as well on the couch as it does on the hunt. Hunted on ungulates, wild boar, fallow deer and roe deer. Energetic and loves everything and everyone. 38 cm and 10 kg (show kilo)   
…..Fearless, curious, independent and easy to learn. Mice and rats, well, the cat does it so well that I don't have to, that's his measure**

**Number of offspring:  
  
 0**

**

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/5f/1e/5f1e5c97-9a18-4ec1-bbaf-7881239722da.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/5f/1e/5f1e5c97-9a18-4ec1-bbaf-7881239722da.jpg)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fb/ef/fbef0a54-815e-45d9-93f6-46b017957a99.jpeg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fb/ef/fbef0a54-815e-45d9-93f6-46b017957a99.jpeg)

## Gullvivebacken's Moses

[Anna Lenjesson  
](mailto:anna.lenjesson@svenskakyrkan.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)Borås  
  
072-529-5564  
  
  
  
**Male dog:**       Gullvivebackens Moses (Pino) ​​SE53514/2018 **Exhibition:**      Excellent CK **Championship/titles:**  **HD result:**      A  
  
       
  
  
         

 **PLL**         N/N FREE    

  
**Mentally described:  
  
**  
  
**Other:**       Pino is a lovely slightly larger and stronger male, height at withers 38-39. He is very kind and calm and has no hunting in him. Pino is good at tricks, arts and obedience and also likes nosework. He is a valued model at HÖÖKS dog products for his beautiful appearance and his ability to pose, which he loves. Pino likes children and always sleeps under the covers. He lives with his family and one of his sons in Borås. **Number of offspring:**    22  
  
  
  
  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/05/e8/05e80697-89e7-428d-b170-073d861786e4.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/05/e8/05e80697-89e7-428d-b170-073d861786e4.jpg)

## Incaline's Aslan

Agneta Riemann  
[http://www.123minsida.se/Walle1](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.123minsida.se/Walle1)    
[agneta101@hotmail.com](mailto:agneta101@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 296 05 30  
Gällivare  
  

**Dog:**     

Inclines Aslan   
SE29504/2010

**Exhibition:**

NORTH UCH

**Championships/Titles:**

SE UCH, FI UCH, NO UCH

**HD results:**

B

**Mentally described:**

MH

**Other:**

Passed aptitude test game track, competes in agility, passed therapy dog ​​test, very good physique, incredible mentality

**Number of offspring:**

13 of which 6 in Sweden

---

### Kråberga farm's Kasper house mouse

Thomas Jost auf der Stroththomasjads@gmail.com  
Dalhemsvägen 15A  
269 36 BÅSTAD  
0733804182  

---

> SE14116/2019  
> PLL hereditary free  
> Excellent at show  
>   

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fb/f7/fbf7b37e-0c9e-47a0-aeee-a6e322826235.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/fb/f7/fbf7b37e-0c9e-47a0-aeee-a6e322826235.jpg)

---

**Leonberget´z Saxo Grammaticus**  

Agneta Wändell  
Degefors  
[agneta@wandellcoaching.se](mailto:agneta@wandellcoaching.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
  
  
  
Male dog: Leonberget´z Saxo Grammaticus  
             SE13448/2017  
  
HD: B  
  
Exhibition: Excellent  
  
MH/BPH BPH  
  
  
Other:          

Saxon is responsive, happy and eager to do things. He competes in nosework and also trains game tracking, obedience and Hoopers. At home he is the family clown. Always ready for pranks and play. Quick to react, but also quick to obey. He is social with both people and other dogs. Always with a twinkle in his eye.

  
  
Number of offspring: 0  
  
  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/55/5f/555f7174-5a10-4a16-8893-09efe67e4c66.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/55/5f/555f7174-5a10-4a16-8893-09efe67e4c66.jpg)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8d/b3/8db302ac-495f-4d6d-a58b-9fb6f5dff5f6.jpeg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8d/b3/8db302ac-495f-4d6d-a58b-9fb6f5dff5f6.jpeg)

## LINDA'S PAWS GANDALF

NAME 

Ann-Sofie Stefansson

E-mail

annsofiestefansson81@gmail.com

Address

Dalavägen 20, Söderbärke, Dalarna, 77760

Phone number

0768488662

Male dog's name

Linda's Paws Gandalf (Rambo)

Reg. number SKK

SE17054/2021

Hip joint status

A

PLL

Hereditary free

Highest ext. merit

Excellent

Miscellaneous

Rambo is a lovely family dog ​​who always wants to participate in all activities. Is calm and has a nice temperament. Likes obedience and being allowed to use the nose.  
  
ED ua (0) (0 + 0)

MH/BPH

MH

  

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/2f/f3/2ff3972c-75b4-4d9b-a509-c8384e398bd8.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/2f/f3/2ff3972c-75b4-4d9b-a509-c8384e398bd8.jpg)

## Myntagården's Jack Af Anton  

Owner: Lena Kaipio.   
Kolsva  
[Carin Stoehr](mailto:Carin.Stoehr@telia.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
073 6820420  

  

**Dog:**     

Myntagården's Jack By Anton  
SE46782/2018 

**Exhibition:**

 Cert

**Championships/Titles:**

  

**HD results:**

  
A  
PLL free

**Mentally described:** 

 BPH with shots

**Other:**

  
All contact takes place through the breeder   
[Carin Stoehr](mailto:Carin.Stoehr@telia.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

**Number of offspring:**   

9

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/dc/78/dc780ed0-31d9-43f3-8c39-0630aca95573.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/dc/78/dc780ed0-31d9-43f3-8c39-0630aca95573.jpg)

## Pepper salt

Owner: Marie Skoglund  
manna-m@telia.com

[Strannajyckens Kennel](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.strannajyckens.se/)  
Phone number: 070-646 21 41Hudiksvall **Male dog:**            Peppersalt SE46593/2016 **Exhibition:**            Excellent Ck / Cert **Championship/titles:** **HD result:**       B/C  **Mentally described:** **Other:**          
    
  
  
  
  
  
  
  
  
  
  
**

The Sixten is a guy of the slightly larger model. A superb rat catcher. He is an incredibly happy dog ​​who likes to be involved in most things. But he is also a master at just lying down and snuggling, preferably under the covers.

**  
**Number of offspring:**    0  
  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/1c/9f/1c9f978b-7c30-4529-a4fe-5d56daab8d9c.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/1c/9f/1c9f978b-7c30-4529-a4fe-5d56daab8d9c.jpg)

## Petter´n Petrasonn By Riverroom

Anne Grete E. Buen  
[http://www.kennelriverroom.com](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.kennelriverroom.com)  
[kennel.riverroom@svorka.net](mailto:kennel.riverroom@svorka.net?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
2460 Osen, NORWAY  
0047 62 444 40 89  
  

**Dog:**     

Petter´n Petrasonn By Riverroom NO54697/11

**Exhibition:**

NO UCH, SEE UCH

**Championships/Titles:**

NJV-12, NO UCH, SEE UCH

**HD results:**

A

**Mentally described:**

  

**Other:**

Patella etc

**Number of offspring:**

6

---

## Pias Hundhage's Skott

Caroline Tranmyren  
[Caroline.tranmyren@hotmail.com](mailto:caroline.tranmyren@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
0706675884  
Veberöd  
  

**Dog:**

Shot  
SE14676/2015

**Exhibition:**

 Very Good

**Championships/Titles:**

  

**HD results:**

A

**Mentally described:**

  

**Other:**

  

**Number of offspring:**

14

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/de/65/de656a1f-4d14-4e80-954d-171cfa1f4c87.png)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/de/65/de656a1f-4d14-4e80-954d-171cfa1f4c87.png)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/23/8c/238c21e5-aeb9-43d2-bb19-df37e2123f0d.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/23/8c/238c21e5-aeb9-43d2-bb19-df37e2123f0d.jpg)

## Piccobello Ferrari's Fenton C&D

Christina Strandberg  
Enköping  
[http://www.dobguns.se](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dobguns.se)  
[dobguns@hotmail.com](mailto:dobguns@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
076 019 57 98  
  
  

**Dog:**

Piccobello Ferrari's Fenton C&D SE33548/2014

**Exhibition:**

World Winner 2017, World Winner 2016,  Junior World Winner 2015 International Champion. Croatian, German VDH, Bulgarian, Russian, Russian Grand, Nordic, Italian, Norwegian, Finnish, Swedish, Estonian, Latvian, Lithuanian, Baltic, Dutch Champion Estonian Junior & Italian Junior Champion. Nordic Winner 2015, German Winner 2017, Benelux Winner 2016, BundesSieger 2016, HerbstSieger 2016.

  

  

**HD results:**

   B

**Mentally described:**

  
   MH  
  

**Other:**

  

  PLL DNA / CLEAR   HD A  
  LP 0/0  
  DNA PLL N/N Clear  
  Cardio Screening UA

37 cm

Short tail

Will be in Sweden for breeding from 1/3-2021 through 31/5-2021

**Number of offspring:**

23

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6a/c4/6ac4f5a8-9c27-4a7c-af38-779ddf516b98.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6a/c4/6ac4f5a8-9c27-4a7c-af38-779ddf516b98.jpg)

## Piece Tjalle Cross wig

Bengt-Åke Bogren  
[bengtake.bogren@telia.com](mailto:bengtake.bogren@telia.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 403 32 99  
Kristianstad  
  

**Dog:**

Piece Tjalle Cross Wig  
SE10458/2011

**Exhibition:**

Always Excellent, BOB many times

**Championships/Titles:**

 SEE UCH, DK UCH

**HD results:**

A

**Mentally described:**

  

**Other:**

  

**Number of offspring:**

21  

---

**PUMBA**

Annika Johansson  
annicajoh72@gmail.com  
Vänersborg

  

**Male dog:**             Pumba SE21826/2019

**Exhibition:**             Excellent  

Championship/titles: SEE VCH  

HD: A  

**Mentally described:**           BPH  

  

 

**Other:**            Social, safe and happy male. Real family dog ​​who loves to be involved in everything we do. Runs loose in the forest but is also in the big city and finds himself at home everywhere.  
  

  

  

**Number of offspring:**       7

  

  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c9/e4/c9e40588-47b6-4c1c-94a2-644484f37267.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c9/e4/c9e40588-47b6-4c1c-94a2-644484f37267.jpg)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/43/b0/43b027b5-437f-41f1-a2bb-9a8759949e8a.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/43/b0/43b027b5-437f-41f1-a2bb-9a8759949e8a.jpg)

## **Ramoya's Hercules**

Cecilia Johansson  
cecilia.bruch@hotmail.com  
070-4492859  
Kungsbacka **Male**         Ramoyas Herkules  **Reg.nr**          SE14950/2017 **Exhibition**        Not exhibited **Championship/Titles** **HD result**        B **Mentally described** **Number of puppies**  8  
  
              
  
  
  
  
  
  
        
  
  

---

**THE KNIGHT'S FABULOUS LION**

Marie Landberg  
_riddarenskennel@hotmail.com  
_Tel:  076 166 94 05  
LAHOLM  
  

**Male dog:  
Knight's Fairytale Lion SE57068/2010  
  
**

**Exhibition:  
**

BOB and BEST IN SHOW Veteran 11/9 2022  SKK National in Falsterbo

**

BOB and BOB VETERAN in Herning DKK 9/11 2018  
(over 95 dogs registered in the breed)

He also became BEST IN SHOW in the Final Ring of the Danish National Breeds that day.

BIG 3a in Group 2 SKK Gothenburg 6/1 2015

BIG 4a in Group 2 SKK Stora Stockholm 15/12 2012

As well as several BOB, BOM and BOB Veteran over the years.

  
**

**Championships/titles:  
**DK uch, DK V-18, DK VV-18, SE uch, SE V-12, SE VV-18.

**HD score:  
**C        

 **PLL:**Clear**  
  
Patella:  
**UA 

**Mentally described:  
**MH

**Other:  
**Born with a stubby tail  
  

Healthy, handsome and incredibly interested in objects!!  
Always out to make life miserable for the pests on the farm!!!  
And the best friend you can imagine.

****Number of offspring:  
****33   

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6d/c4/6dc4fb09-4b67-4860-b8fe-64f3f2670593.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/6d/c4/6dc4fb09-4b67-4860-b8fe-64f3f2670593.jpg)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/76/f2/76f261ba-67db-4f2c-ab6f-1ef088b0c1ab.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/76/f2/76f261ba-67db-4f2c-ab6f-1ef088b0c1ab.jpg)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/72/8d/728d3316-d821-4378-ac69-77166969ce0c.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/72/8d/728d3316-d821-4378-ac69-77166969ce0c.jpg)

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8e/c7/8ec71f35-e40f-4d7c-a661-3d4bd64b4cfe.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8e/c7/8ec71f35-e40f-4d7c-a661-3d4bd64b4cfe.jpg)

## Spero Dino

[Patrik Lundblad jan.patrik.lundblad@telia.com](mailto:patrik.lundblad@tietoevry.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)Tel. no:  073 438 21 12  
[Svedala](mailto:patrik.lundblad@tietoevry.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
  

**Dog:**                 

Spero Dino SE30100/2018

**Exhibition:**

Excellent

**Championships/Titles:**

  

**HD results:  
**

A

**Mentally described:**

**Other:**

Since we have already received a mating request and are now waiting to see if the mating attempt was successful, we have also obtained a veterinary certificate regarding the testicle certificate.  

**Number of offspring:**  

0

  
  
  
  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/41/df/41dfbfdf-8837-4a99-a4e4-1b9f21d2557e.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/41/df/41dfbfdf-8837-4a99-a4e4-1b9f21d2557e.jpg)

## Stallbacka's Värmland's Wood

Johnny Svensson  
[Johnnysvensson47@gmail.com](mailto:johnnysvensson47@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 211 07 50  
Korskrogen  
  

**Male dog:** Stallbacka's Värmland's Wood

 SE57969/2015

**Exhibition:**

 Cert, Excellent

**Championships/Titles:**

  

**HD results:**

B

**Mentally described:**

  

**Other:**

  

**Number of offspring:**

11

---

**PROUD EBB'S NIKE NIKLAS BY THE GIRL**

[Erika Karlsson](mailto:kennel@stoltaebbas.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
Töreboda  
Tel: 076-321 65 05  
  

**Male dog:  
Proud Ebbas Nike Niklas af Jäntan SE56965/2018  
  
**

**Exhibition:  
Excellent with CK  
  
**

**Championships/Titles:  
  
**

**HD score:  
B**         

**PLL:  
Clear  
  
**

**Mentally described:  
  
**

**Other:  
Hip joint status read with A+B  
  
**

**Number of offspring:  
5**  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/7d/04/7d04f599-15ac-4e57-89d6-93061e627068.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/7d/04/7d04f599-15ac-4e57-89d6-93061e627068.jpg)

---

**PROUD EBBAS MY MIMER OF YRSA**

[Erika Karlsson](mailto:kennel@stoltaebbas.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
Töreboda  
Tel: 076-321 65 05  
  

**Male dog:  
Proud Ebba's Min Mimer by Yrsa SE52218/2018  
  
**

**Exhibition:  
Best Male 3 Excellent with CK  
  
**

**Championships/Titles:  
  
**

**HD score:  
B**         

**PLL:  
Clear  
  
**

**Mentally described:  
  
**

**Other:  
  
  
**

**Number of offspring:  
4**  

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8c/6b/8c6bf5ad-5e3f-4a49-b2f7-6e2b90eba95d.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8c/6b/8c6bf5ad-5e3f-4a49-b2f7-6e2b90eba95d.jpg)

---

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d9/81/d981e9ec-08b8-4a82-9a2c-6f53feccf23f.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d9/81/d981e9ec-08b8-4a82-9a2c-6f53feccf23f.jpg)

## Super duper Elvis

Annette Andersson  
[http://www.anderssonsgardshundar.se](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.anderssonsgardshundar.se)  
[annette.superdupers@gmail.com](mailto:annette.superdupers@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 2232525  
Skövde  
  

**Dog**

Superdupers Elvis  
SE14079/2016

**Exhibition:**

BOB

**Championships/Titles:**

SEE UCH

**HD results:**

B

**Mentally described:**

  

**Other:**

  

**Number of offspring:**

3

.

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/a6/f7/a6f7fc17-a730-4646-a817-a7a94b37221e.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/a6/f7/a6f7fc17-a730-4646-a817-a7a94b37221e.jpg)

## Super dupers Sigvard

Berit Lindenger  
[berit.lindenger@gmail.com](mailto:berit.lindenger@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
0706442818  
Hindås  
  

**Dog**

Superdupers Sigvard SE39664/2013

**Exhibition:**

Ck

**Championships/Titles:**

  

**HD results:**

 B

**Mentally described:**

BPH

**Other:**

ED ua, patella ua, eye ua, HD index 98, correct bite, allergen tested low value, BPH: bulletproof, open , fearless

**Number of offspring:**

16

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/1a/65/1a65d420-37cc-4f18-9565-166c4f33142c.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/1a/65/1a65d420-37cc-4f18-9565-166c4f33142c.jpg)

## Swefarm's King of the Feast

Agneta Riemann  
[http://www.123minsida.se/walle1](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.123minsida.se/walle1)  
[agneta101@hotmail.com](mailto:agneta101@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 296 05 30  
Gällivare  
  

**Dog:**

Swefarms Kalaskungen  
SE23776/2016

**Exhibition:**

  

**Championships/Titles:**

SE UCH, FI UCH

**HD results:**

B

**Mentally described:**

BPH

**Other:**

Gdk facility test game track, passed on permo bear test

**Number of offspring:**

0

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/48/f3/48f3b790-a019-438a-82c2-8da5fda08846.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/48/f3/48f3b790-a019-438a-82c2-8da5fda08846.jpg)

## Tamora's Always Action

Jennie Medvall  
[jenniemedvall@gmail.com](mailto:jenniemedvall@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
 073-7767959  
Roman monastery

**Dog:**     

  Tamora's Always Action SE49212/2016

**Exhibition:**

VG

**Championships/Titles:**

  

**HD results:**

A

**Mentally described:**

  

**Other:**

Sammi is a very nice dog who fits in all situations. Social and prefers to always be with and be close.

**Number of offspring:**      

16

  

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/e0/93/e093f8b6-7291-4f24-97c6-2f88d0f10745.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/e0/93/e093f8b6-7291-4f24-97c6-2f88d0f10745.jpg)

## Tamora's Mr. Maxmillian

Helena Afvander  
[helena.afvander@gmail.com](mailto:helena.afvander@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
070 585 40 97  
Hässelby  
  

**Dog:**

Tamora's Mr Maxmillian  
SE57619/2013

**Exhibition:**

VG

**Championships/Titles:**

  

**HD result:  
PLL**        

B  
  
Clear  
  

**Mentally described:**

BPH

**Other:**

A super kind guy who works as a therapy/activation dog with dementia. Very social, loves everything and everyone.

**Number of offspring:**

9

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/59/ff/59ff870d-6ad7-4c27-80cb-b3467b222ebd.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/59/ff/59ff870d-6ad7-4c27-80cb-b3467b222ebd.jpg)

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/58/30/5830b390-abcd-4b58-8399-618152abc0e0.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/58/30/5830b390-abcd-4b58-8399-618152abc0e0.jpg)

## Team Campbell's Brage

  

**Sture Thörngren**  
**Louise Campbell**  
[**louise@teamcampbell.se**](mailto:louise@teamcampbell.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
**Enköping**  
  

**Male dog:  
Team Campbell's Brage SE58875/2019  
  
**

**Exhibition:  
Excellent, Ck, R-cert, NORDIC R-cert  
  
**

**Championships/Titles:  
  
**

**HD score:   
B**        

 **PLL:  
CLEAR** 

**Mentally described:  
  
**

**Other:  
Felix, as he is called everyday, is a very nice and positive male who loves to train whatever he is offered. He has a nice energy during training and always has good focus regardless of the weather, new places or disturbances. Felix did a fantastic BPH where he really showed what a fearless, happy, confident and curious dog he is. He is always kind and happy when meeting other dogs and people. Felix lives in the country with his Shepherd friend and there he usually goes for walks, so not much hunting in him. He has a scissor bite, complete dentition, X-rayed HD B (individual index 105 18/8-22), ED ua.** 

****Felix has an extra spur on his right hind paw.****

**All contact is via breeder Louise Campbell, [louise@teamcampbell.se](mailto:louise@teamcampbell.se?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
  
  
**

**Number of offspring:  
0**

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/cf/fc/cffcad29-efeb-45a0-a66f-03605bb27815.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/cf/fc/cffcad29-efeb-45a0-a66f-03605bb27815.jpg)

## Team's Sambo

Annette Hansson  

[anettehansson59@hotmail.com](mailto:anettehansson59@hotmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
073 593 60 04  
Halmstad

  
  

**Dog:**         

Team's Sambo  
SE21478/2019   
 

**Exhibition:**

4 months Ex. Bee puppy HP

6 months Ex. HP

Exterior assessment VG

**Championships/Titles:**

  

**HD result:  
PLL**        

A  
N/A  
  

**Mentally described:**

  

**Other:**

**Number of offspring:**                            

25

  

---

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8a/0d/8a0d25b2-176a-4e9c-8aa7-6c71a04941f7.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/8a/0d/8a0d25b2-176a-4e9c-8aa7-6c71a04941f7.jpg)

## Wopper's Okidoki

  
Bengt Ström  
[bengt.o.strom@gmail.com](mailto:bengt.o.strom@gmail.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  
0702968789  
Hedemora  
  

**Dog:**

Woppers Okidoki  
SE48377/2016

**Exhibition:**

BOB, BIM

**Championships/Titles:**

SEE UCH

**HD  
Index**         

A  
114 on 21/3-2022

**Mentally described:**

BPH

**Other:**

A little about Wopper's Okidoki (Otto). He is an active dog. Likes to track and look for objects. Loves mountain hiking and being in the canoe. Likes to snuggle on the sofa, kind with other dogs. His data inbreeding rate 0.0%. HD index is 109.  
PLL free  
Wither height 35.5 cm weight 10.3 kg. Otto is at the service of PLL-free bitches and for bitches with long tails that meet the RDSG's recommendations for breeding animals. However, the lowest HD-B on the hips where the pairing index is at least 105.

**Number of offspring:** 

12

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/43/b6/43b6c4c6-6c3e-4aea-abaf-6583b53e605c.jpeg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/43/b6/43b6c4c6-6c3e-4aea-abaf-6583b53e605c.jpeg)

## ÅSBY HEIGHTS CESAR

NAME

Agneta Angman

E-mail

akacia55@hotmail.com

Address

Helgarö Nybblelund 1, Strängnäs, 64592

Phone number

(073) 617-4339

Male dog's name

Åsby Höjdens Cesar

Reg. number SKK

SE 21709/2021

Hip joint status

A

Eye bright UA

Highest ext. merit

Excellent

Miscellaneous

A very nice guy who always wants to do his best, is nice and social with everyone. Has a lot of motor but has no problem unwinding and taking it easy likes to cuddle and snuggle and lie close. We have tried several branches of training, he thinks everything is fun and is also good at it.

MH/BPH

BPH


