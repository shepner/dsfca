---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

Source: [Redigerare](https://gardshund-com.translate.goog/avel/enk%C3%A4ter-38399431?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

# QUESTIONNAIRES

**QUESTIONNAIRES**  

It is very important for the breed club to have an ongoing evaluation of the breed's status, to have the opportunity to change breeding recommendations, RAS and to be able to alert both breeders, other interested in breeding and, for example, conformation judges to any deviations.  
  
To help us with the evaluation of submitted results, the breed club, the working group for breeding issues, a veterinarian and the behavior committee have.


!!! TODO "translate these forms"

[Mentality](https://form.jotform.com/220053078086048)

[Health](https://form.jotform.com/220034012585039)

[Exterior](https://form.jotform.com/210606688963062)
