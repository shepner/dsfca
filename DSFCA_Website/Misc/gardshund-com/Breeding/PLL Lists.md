---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

# PLL Testing (??)

**[This is how you PLL test your dog](https://gardshund-com.translate.goog/avel/så-här-gör-du?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)**

**Results for [formula PLL  
](https://gardshund-com.translate.goog/avel/pll-formulär?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)**

**[Do](https://gardshund-com.translate.goog/avel/pll-formulär?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) you have questions about PLL?  **send an email to** [Ewa Osterman](mailto:galantskennel@telia.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)**

  

[We follow the SKK's limitation of what can be considered as Hereditarily exempt.](mailto:galantskennel@telia.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)This means that 3 generations apply for hereditary exemption, future generations need to have their DNA tested again.Read more at: [_https://www.skk.se/sv/nyheter/2021/3/hereditart-friforklarad-begransas-till-3-generationer/_](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/sv/nyheter/2021/3/hereditart-friforklarad-begransas-till-3-generationer/)

  

---

- [PLL List Sweden](http://files.builder.misssite.com/db/22/db223bc5-89fe-4a33-a002-ce0b95d6901b.pdf)
- [PLL List Denmark](http://files.builder.misssite.com/b2/2f/b22fbb77-9b07-4e34-84d3-7aeaeb4c99f3.pdf)
- [PLL List Finland](https://docs.google.com/spreadsheets/u/1/d/e/2PACX-1vSB52BknsL4eOei9ABhN9jW9t9qXP1d15eR8tIgdL5AdfeY-FcAlo6-pu8fdtv9ZA/pubhtml?gid=986971059&single=true)
- [Dansk/Svensk Gårdhundeklub](https://www.dsgk.dk/news.asp?ID=79068) (this is a link to the news page)

---

[![The lens of the normal eye is suspended by zonular threads that attach around the lens and in the ciliary muscle.  In PLL, the zonular threads that hold the lens in place break down.](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d3/b9/d3b998a8-c329-4e6d-9ed6-e33291121942.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/d3/b9/d3b998a8-c329-4e6d-9ed6-e33291121942.jpg)

#### **Primary lens dislocation (PLL).**

Background  

There is an eye disease in dogs called primary lens luxation (PLL). It is found in many dog ​​breeds, but is more common in some terriers and terrier-like breeds. The disease leads to great suffering for the affected dogs and must be seen as serious.

The lens of the normal eye is suspended by zonular threads that attach around the lens and in the ciliary muscle (see picture). In PLL, the zonular threads that hold the lens in place break down. The lens can then be moved completely or partially out of its position. This displacement of the lens is called luxation. PLL is very painful and causes blindness when the lens is moved out of position.  

Primary lens dislocation (PLL) is hereditary, but lens dislocation can also occur through, for example, trauma or chronic inflammation, so-called secondary lens dislocation. Therefore, lens dislocation can also affect genetically free dogs. It is not possible to distinguish these two types of lens dislocation using symptoms. The first clinical signs of PLL are often detected at around 20 months of age, and complete lens detachment usually occurs between the ages of 3 and 8 years.

PLL is thus hereditary and is caused by a mutation in the ADAMTS17 gene. This gene codes for a protein that is part of the building blocks of the zonular filaments. When the gene is mutated, it produces a defective protein. PLL is inherited in an autosomal recessive manner. This means that affected dogs have received a mutated gene (allele) from both the mother and the father. Therefore, both parents must carry at least one mutated allele.

In Sweden, there is a single case of lens luxation in a Danish-Swedish farm dog reported to the Breed Club, but it has not been determined that it is the hereditary form. During the autumn, several dogs were retested for PLL and it has emerged that some dogs that are closely related to each other are carriers of PLL (N/PLL). 

Validated genetic tests for DSG for ADAMTS17 mutation are available in many veterinary clinical diagnostic laboratories. According to several of these laboratories, approximately 2 - 20% of carriers (N/PLL) may also develop PLL. These data come from a study published in Investigative Ophthalmology & Visual Science (Farias et al. 2010).

The Breed Club has been in contact with the lead author of this article, Dr Cathryn Mellersh. She says that her research group has followed up this 2010 study with countless contacts with eye specialists and that they have seen no increased risk for (N/PLL) carriers to develop disease in any breed other than miniature bull terriers and Lancashire  
 heelers   . The increased risk in these two breeds is likely due to additional unknown genetic factors.  
These observations are unfortunately not published.

There is probably no increased risk in Danish-Swedish farm dogs for carriers (N/PLL) to develop disease. To fully ensure this, a study with eye scanning of older wearers would need to be done. Since we have so few carriers in the breed, such a study is difficult to carry out. 

The background to PLL in Danish-Swedish farm dogs

Genetic diseases such as PLL occur when an unfavorable change (mutation) occurs in the genetic material. This has usually happened many generations before the disease is expressed. Sometimes it is necessary for the mutation to be present in both the mother and the father for the disease to be expressed in an offspring. We then speak of recessive inheritance. This allows the disadvantageous change to be inherited without the dogs becoming ill. With a large population, there is little risk that the predisposition is duplicated and the mutation can hide for many generations.

The creation of our dog breeds is actually a genetic design project carried out by us humans and it is still very much ongoing. A few individuals have been selected to found each new race because they have had desirable characteristics. When a small number of individuals are selected and backcrossed to each other (inbreeding) to further enhance traits, amounts of genetic variation are lost. This means that the dogs in the new breed will be very similar to each other and there will be few variants of each genetic predisposition. The risk of doubling disease predisposition is then also much greater. If one of the selected dogs happens to carry a disease predisposition, this too is unintentionally reinforced and enriched in future generations.

The more breeding animals within a breed, i.e. the more unique individuals that mate with each other, the more diluted the gene pool becomes and disease predispositions are not expressed as strongly. You can see that breeds with more founders and less inbreeding are not at all as affected by genetic diseases.  
  
The genetic eye disease PLL, which is caused by a mutation in the gene ADAMTS17, is found in about 30 different breeds and has probably existed in the dog species even before the breeds became breeds. The Danish-Swedish farm dog was approved as a breed in 1987 in Sweden, and the predisposition to PLL has of course been present in the breed since its inception. The first known cases of PLL appeared in Finland in 2015. A genetic mapping was done and several carriers were found in the family.

In Sweden, there are so far no known cases of PLL, but it was recently discovered that a male dog that was used a lot in breeding and had many offspring was a carrier. This was the prelude to the mapping that RDSG is now carrying out. Sweden and Denmark have joint responsibility for the Danish-Swedish farm dog breed and we will also carry out a mapping project together with the Danish breed club DSGK.

  

The mapping of PLL was completed and new breeding recommendations were adopted in February 2022.

We are still not aware of any cases of dogs suffering from PLL in Sweden. One dog has been reported with a diagnosis of PLL, but both parents were DNA tested clear so the cause of lens dislocation was different. We are not aware of more dogs suffering from PLL in the world than the five dogs from 2015 in Finland. We are still not aware of any carrier anywhere in the world being diagnosed with PLL.

The working group for breeding issues has administered the PLL list. On the list there are (2022-04-23) 334 DNA-tested dogs, of which 15 are carriers, which is 4.5% of the tested dogs. On the Finnish list, a larger proportion are carriers (14%), but in Finland known carriers have always been used in breeding and the consequence is that there will be more carriers in the population.

The vast majority of the carriers on both the Swedish and Finnish lists are descended from Son Mik's Ydun (S49362/2001) or Kvik (DK17273/99).

Number of tested dogs and carriers in Sweden on our list is not a complete picture. Some breeders test and report to the breed club, others only report to the Finnish list. Some refrain from sharing their test results with the club, some test and establish their own unofficial lists and finally there are breeders who have not tested a single dog and will not do so. It shows quite nicely how difficult it is to unite breeders on different issues.

Breeding recommendations:  
  

o Continue to encourage DNA testing for PLL and reporting to the breed club list, but not require testing to obtain a puppy referral.

o Strongly recommend testing dogs that have PLL carriers or untested close relatives of PLL carriers in their lines, if there are no free-tested dogs in between.

o Strongly recommend testing dogs that have Son Mik's Ydun (S49362/2001) or Kvik (DK17273/99) in their lines, if there are no free-tested dogs in between.

o Carriers may only be used in breeding together with dogs that are DNA-tested free or are hereditary free.