
[Male dog list form](https://form.jotform.com/210302652150034)

Welcome to register your male to our list of males available for breeding.  
  
In order to get your male entered on RDSG's male dog list, the following is required:

-   The owner is a member of RDSG.
-   [RDSG's recommendations](https://gardshund-com.translate.goog/avel/avelsrekommendationer-35478600?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)  for breeding animals must be followed.
-   The male must be x-rayed with HD grade A, B or C.
-   The male must be 24 months old at the time of mating, preferably older.
-   The male must be exhibited with at least VG at 18 months at the earliest.
-   A picture of the male must be attached. It is desirable that the photo is taken of the dog in profile, preferably standing.