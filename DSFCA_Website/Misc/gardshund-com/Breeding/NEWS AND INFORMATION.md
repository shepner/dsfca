Source: [Nyheter och information | gardshund.com](https://gardshund-com.translate.goog/avel/nyheter-och-information-37611484?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

## _NEWS AND INFORMATION_

  
  
Here, information will be continuously updated for breeders, those interested in breeding and other farm dog owners.

---

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c5/5b/c55bb397-7b16-4f1e-a7cc-815107396c9a.JPG)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://files.builder.misssite.com/c5/5b/c55bb397-7b16-4f1e-a7cc-815107396c9a.JPG)

---

**Compendium of Judges** 

**This year there will be a revised compendium on the occasion of the judges' conference that will take place in 2023.  
There are no major changes, new pictures and no changes in the comments, in addition, of course, the standard established by the FCI in connection with the breed being internationally approved with the new compendium.**

  

---

CASTRATION  

It has become more common for dogs to be castrated in Sweden, that is, to surgically remove the dog's testicles or fallopian tubes, or to chemically add a hormone preparation.

 **The Swedish Kennel Club does not believe that castration should be performed routinely.**

It reduces the breeding base and leads to a poorer opportunity to evaluate dogs for breeding, which is needed to be able to influence mentality and other characteristics through well-planned breeding. We are keen on our Swedish tradition of training and educating our dogs to ensure a good relationship between dog, dog owner and the outside world.  
  
**Leads to reduced breeding base**

Surgically neutering a dog means removing the testicles or fallopian tubes and thus making the dog sterile. With fewer dogs available for breeding, the breeding base is shrinking, which poses challenges for long-term and sustainable dog breeding, especially in dog breeds with small populations.

## Less opportunity for breeding evaluation

It is worrying if a large number of dogs are neutered at an early age because it means that the opportunity to fairly evaluate the dogs' characteristics is lost. It can, for example, be about health, mentality, exterior and work ability.  
  
**Mentality must change with breeding**

Changing the mentality and other characteristics of dogs should be done through well-planned breeding work. Trying to change behavior through routine castration is therefore not supported by SKK. It is important that breed clubs and breeders provide puppy buyers with relevant information about the mentality, behavior and activation needs of the dog breed in question before purchase to prevent relationship problems. 

---

### _BREEDING CONFERENCE_

### _Rotebro 20 March 2022_

  

.

The breed club for the Danish-Swedish farm dog invited its members to an all-day breeding conference.

There were representatives from about 12 different kennels and several female owners who have plans for future breeding and also several male dog owners who participated. 

It was a combined lecture/discussion session where everyone could contribute their experiences, thoughts and reflections, so recounting in detail will be difficult here, it will be more of a summary of the day in broad terms. 

The lecture by Maija Eloranta from the Swedish Kennel Club's breeding committee was about:

-How to choose breeding animals? -with RAS as a practical breeding tool, based on our breed of farm dog.

-How should we preserve the genetic variation?

-How can we increase the breeding base?

-How much genetic variation is there in the breed, and how can we measure and estimate it?   
On average, 49% more females are used in breeding per year than males, proposals were discussed on how to get more different males into breeding, to increase the effective population size and thus reduce inbreeding. That we breeders, for example, would rather use a litter brother or a half brother or maybe even the father of an otherwise popular male dog. How important it is for the breed that many males are kept uncastrated as they can contribute with a litter of puppies when they are older. 

-What is the risk of overuse of individual breeding animals?   
We may end up in new bottlenecks where we lose important genetic variation and diseases that may show up later. If that breeding animal then has many children/grandchildren who inherit the disease, the number of sick animals increases unnecessarily. 

- Age of bitch at first litter?   
That the farm dog is often only mature around 3-4 years, as it matures a little later than many other breeds. 

-How many puppies should a male dog have as a maximum?   
That according to SKK you should then calculate that the puppies born during the male's active years in breeding are a maximum of 5% of the total farm dog puppies born during these years. Assume 4 years in breeding, that 100 farm dog puppies are born in total in Sweden each year, 5% of 400 puppies are 20 puppies and that these 20 puppies are then an absolute upper limit, it is desirable to have fewer puppies per breeding male but that we get more different male dogs in breeding. 

-How harsh or not harsh should we be in our breeding selections considering the genetic variation?  
And DNA tests. Should they be used because they are available or when they are important tools in the breeding work of our breed? Here, together with Maija Eloranta, we came to the conclusion not to test because everything is a fairly sensible approach. And to only use the tests that are validated for the farm dog when they possibly need to be done because otherwise the possible selection in the breeding selection can actually harm more than help. A DNA test does not provide answers to defects or characteristics with complex inheritance, such as allergies, HD. 

-How to choose males for breeding?   
It was discussed how we breeders should find genetically valuable individuals. That now that we have gone down to health program 1 again, we can get a larger selection of breeding animals with less focus on HD, and if we believe it will be used and exploited by the breeders. And what does that entail?

That we should choose males that have relatives with a long life and good health.

With less relatedness, there is a lower probability that the defective genes will be duplicated, thus a chance for healthier dogs. 

-If the inbreeding rate can be underestimated? It is basically the same in all breeds as SKK's breeding data only calculates on 5 links, and the farm dog database Muttington's calculates on 6 links, but if you take into account all known dogs backwards, each individual's degree of inbreeding is much higher. 

-What should be prioritized in the breeding work? Here we participants came to the conclusion that the most important points are:

- to breed dogs with a breed-typical temperament that works in today's society.

-The genetic variation.

-Longer generation intervals, to dare to breed with older dogs, both bitches and males. 

Maija thought that we breeders, activists and also the breed club should get more information about what a fantastic breed we have which is actually a very healthy dog ​​breed and which is represented in so many different dog sports.

 Sofia Westergren

  

---

#### 

**Situation report Cystenuria November 2020  
**

**Cystinuria in a Danish-Swedish farm dog**

Cystinuria is a disease that often leads to recurrent urinary stone formation. The reason is that the return transport in the kidneys from urine to blood of a special amino acid, cystine, does not work. This leads to increased amounts of cystine in the urine. Cystine has low solubility in acidic urine and crystals and stones form. This can lead to blockage of the ureters with severe pain, kidney failure and, in the worst case, death as a result. There is still no cure for cystinuria, but with the help of special feed and medication it is possible to reduce the re-formation of cystine stones and even in some cases to dissolve existing crystals. For affected male dogs, castration is recommended, which usually results in reduced stone formation. If the dog has been diagnosed with cystinuria, lifelong treatment applies.

The genetic background of cystinuria has been elucidated in some breeds. The disease is divided into four classes;

Type IA: Caused by a mutation in the SLC3A1 gene and has an autosomal recessive inheritance pattern.

Type II-A: Also caused by a mutation in the SLC3A1 gene but has an autosomal dominant pattern of inheritance.

Type II-B: Caused by a mutation in the SLC7A9 gene and inherited in an autosomal dominant manner.

Type III: Here mutation and inheritance are still unknown. Type III is hormone dependent and affects exclusively intact adult male dogs.

At the beginning of February, a number of Danish-Swedish farm dogs with cystinuria, some healthy control animals and healthy close relatives were tested for the mutation in the SLC7A9 gene that causes cystinuria in several breeds. All dogs were negative for this mutation. The tests have been done in collaboration with Laboklin and a veterinary medical research group that works with cystinuria. Danish-Swedish farm dogs have previously tested negative for the two known mutations in the SLC3A1 gene that cause cystinuria in some other breeds. It is now completely established that cystinuria in Danish-Swedish farm dogs is not caused by any of the known mutations.

The breed club's intention is now to go ahead and seek funds to be able to be included in a project on hormone-dependent cystinuria (cystinuria type III) which occurs, among other things, in the Kromfohrländer and Irish terrier breeds. This type of cystinuria can only develop in the presence of the male dog's sex hormones and only affects intact male dogs. The inheritance is still unknown but is suspected to be complex or autosomal recessive with probable environmental influence. The research is difficult because bitches never get this type of cystinuria and the males are probably just predisposed, ie not even all genetically loaded males get sick. The aim of this project is to be able to develop a genetic test to be able to identify predisposed dogs and carriers. Since all cases of the disease that have occurred so far in Danish-Swedish farm dogs are intact males, the researchers' assessment is that it is very likely this type of cystinuria that affects Danish-Swedish farm dogs and we are welcome to participate in the project. The project is led by Prof. Dr. Urs Giger and Prof. Dr. Tosso Leeb.

Prof. Dr. Urs Giger is a veterinarian, researcher and world-leading expert in hereditary metabolic diseases in pets. He has researched cystinuria on a genetic, biochemical and clinical level for many years and has, among other things, developed genetic tests for cystinuria for several dog breeds. Prof. Dr. Tosso Leeb is an expert in molecular genetics and DNA sequence analysis.

We are also in contact with Prof. Åke Hedhammar, veterinarian and researcher at SLU and veterinary medical advisor at SKK for advice regarding participation in this research project.

Cystinuria in Danish-Swedish farm dogs is still considered a disease of unknown inheritance and SKK's breeding recommendations continue to apply:

· Do not use dogs with cystinuria in breeding.  
· Do not repeat the same parental combination that left offspring with cystinuria.  
· Take care when breeding siblings, parents and offspring of sick dogs.  
· In addition to this, it is recommended to breed with a low inbreeding rate to reduce the risk of any recessive disease predisposition doubling.

It is of course also very important that breeders and male dog owners are open and inform each other about cases of cystinuria in their lines to avoid possible duplication of disease predisposition.

We have only become aware of one new case of cystinuria in DSG in 2020.

We ask for continued help with reporting cases of cystinuria to the Breed Club for mapping relationships between affected animals and for possible future sampling. If you have a dog affected by cystinuria, notify the breed club via e-mail to the group for breeding issues; [arbetsgruppavel@gardshund.com](mailto:arbetsgruppavel@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) .

Don't forget to send a veterinary certificate.

  
  

**Status report April 2020 - Cystinuria at DSG**  

#### 

At the beginning of February, a number of Danish-Swedish farm dogs with cystinuria,  
some healthy control animals and healthy close relatives were tested for the mutation in the SLC7A9 gene  
that causes cystinuria in several breeds. All dogs were negative for this mutation.  
 The tests have been done in collaboration with Laboklin and a veterinary medical research group  
that works with cystinuria. Danish-Swedish farm dogs have previously tested negative for the two known mutations in the SLC3A1 gene that cause cystinuria in some other breeds. It is now completely established that cystinuria in Danish-Swedish farm dogs is not caused by any of the known mutations.  
  
  

The breed club's intention is now to go ahead and seek funds to be able to be included in a project  
 on hormone-dependent cystinuria (cystinuria type III) which occurs, among other things, in  
the Kromfohrländer and Irish terrier breeds. This type of cystinuria can only develop  
in the presence of the male dog's sex hormones and only affects intact male dogs.  
 The inheritance is still unknown but is suspected to be complex or autosomal recessive  
with probable environmental influence.  
The research is difficult because bitches never get this type of cystinuria and the males  
 are probably just predisposed, ie not even all genetically loaded males get sick.  
The purpose of this project is to be able to develop a genetic test to be able to identify  
 predisposed dogs and carriers. Since all cases of the disease that have occurred so far  
in Danish-Swedish farm dogs are intact males, the researchers' assessment is that it is very likely  
 this type of cystinuria that affects Danish-Swedish farm dogs and we are welcome to participate  
 in the project. The project is led by Prof. Dr. Urs Giger and Prof. Dr. Tosso Leeb. Prof. Dr. Urs Giger is a veterinarian, researcher and world-leading expert in hereditary metabolic diseases  in pets. He has researched cystinuria on a genetic, biochemical and clinical level for many years  and has, among other things, developed genetic tests for cystinuria for several dog breeds. Prof. Dr. Tosso Leeb is an expert in molecular genetics and DNA sequence analysis.  
  
  
  
  
  
We are also in contact with Professor Åke Hedhammar, veterinarian and researcher at SLU and veterinary medical  
advisor at SKK for advice regarding participation in this research project.  
  
Cystinuria in Danish-Swedish farm dogs is still considered a disease of unknown inheritance,  
 and SKK's breeding recommendations continue to apply:  
**·**   Do not use dogs with cystinuria in breeding.  
**·**   Do not repeat the same parental combination that left offspring with cystinuria.  
· Be careful when breeding siblings, parents and offspring of sick dogs.  
· In addition to this, it is recommended to breed with a low inbreeding rate to reduce the risk of  
 any recessive disease predisposition doubling.  
It is of course also very important that breeders and male dog owners are open and  
inform each other about cases of cystinuria in their lines to avoid possible duplication of disease predisposition.  
  
We ask for continued help with reporting cases of cystinuria to the Breed Club for  
mapping relationships between affected animals and for possible future sampling.  
If you have a dog affected by cystinuria, notify the breed club via e-mail to the group for breeding issues;  
  
[arbetsgruppavel@gardshund.com](mailto:arbetsgruppavel@gardshund.com.?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) [.](mailto:arbetsgruppavel@gardshund.com.?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)   Don't forget to send a veterinary certificate.