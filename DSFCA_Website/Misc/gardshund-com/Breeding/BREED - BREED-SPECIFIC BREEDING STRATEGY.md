Source: [RAS | gardshund.com](https://gardshund-com.translate.goog/avel/ras-35478586?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)



**BREED - BREED-SPECIFIC BREEDING STRATEGY**   

_At SKK/AK's meeting on November 16, 2021, it was determined_ 

_**RAS** for Danish-Swedish farm dog._

_**Information regarding health programs**_ 

_

From 16 November, health program level 1 applies to Danish-Swedish farm dogs.  
This means that pairings made after this date are affected by the decision.  
We are moving from health program level 2  
to level 1.

Formally, SKK's central board CS could overturn the decision, but that is not a likely scenario.

_

Final confirmation is effected by the CS at one of their upcoming meetings. Those meetings are not yet scheduled for 2022.

In this case, we make the assessment that CS will not go against the decision.



**Statement on revised** **RAS** **for Danish-Swedish farm dog** The undersigned has reviewed the club's proposal for **RAS** , which is very ambitious and addresses and discusses everything significantly concerning the breeding of the breed. They prioritize the most important measures, especially regarding the importance of genetic variation for the survival of the breed, and have wise strategies for this. The breed has few health problems, but the most important ones are identified and breeding recommendations are adapted to have a good effect without killing too many individuals. The health program for HD at level 2 that has been in place for many years has been evaluated and the conclusion is that level 1 would be more appropriate. This has also been applied for with SKK. In summary, the club has done an excellent **RAS**  

with well-founded conclusions and recommendations that help the breeders achieve the set goals for the breed.   
  
Mariefred 2021-11-09  
Elisabeth Rhodin
