---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

[rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/avel/s%C3%A5-h%C3%A4r-g%C3%B6r-du?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

# 202304220745

Testing can be done via different laboratories. The breed club's advice is to test via Laboklin (laboklin.com), which has proven to be fast and reliable.

A blood sample taken by a veterinarian, who can certify the dog's identity, is required for the test. The blood sample must be at least 1mL of blood in a purple EDTA tube. Some clinics arrange everything - other clinics require you to "pre-purchase" your analysis from Laboklin. Check with your clinic how they do it.

Please bring SKK's DNA referral, so that your dog's test results can possibly be officially registered with SKK in the future. [https://www.skk.se/sv/SKK-klinikwebb/DNA-test/Tester-som-SKK-registrarar/](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/sv/SKK-klinikwebb/DNA-test/Tester-som-SKK-registrerar/) 

  

  

**How do I go about DNA testing my dog?**  

**1.**       
 Call your veterinary clinic and see if they can help you test your dog, ask if they can take your dog's blood.  

(Swab tests are possible, but not recommended as there is a risk that the sample does not contain enough DNA for the test to be carried out, and the dog must be isolated according to special recommendations before sampling).

**2.**       
 Order test kit from **Laboklin** **-** [https://www.laboklin.co.uk/laboklin/](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.laboklin.co.uk/laboklin/)   The test is paid for when you order it online. Detailed instructions for online ordering at Laboklin are below.

**3.**    
  Book an appointment with your veterinarian.

**4.**       Print the referral from SKK so that the test can be registered at SKK in the future, [https://www.skk.se/sv/upfodning/halsa/dna-tester/ovriga-tester/](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.skk.se/sv/uppfodning/halsa/dna-tester/ovriga-tester/)

**5.**   
   Go to the vet, bring pre-ordered test kit if your vet does not provide that service, pedigree and SKK referral. The clinic takes the test correctly, checks the dog's identity, and fills in all the documents you brought, and the clinic sends in the sample.

**6.**  
  After a few days, when you receive your result, you send it in to the breed club's breeding council. Results N/N (Clear) must also be submitted.

**7.**    
  If your dog is affected (PLL/PLL) or carrier (N/PLL) you must inform the owners of the parents, full siblings and offspring.

**8.  
**  Submit test answers and referral to SKK.

  

**Pre-order PLL test at Laboklin**  

**1.**        
 Go to [https://laboklin.com/se/products/genetics/](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://laboklin.com/se/products/genetics/)

When you enter Laboklin for the first time and want to register as a breeder,they request that you scan your kennel certificate. When you have registered your details, you have to wait for an approval (by email) from Laboklin before you order your test. Then you can get a certain discount.  

**2.**        
 Click on "Genetic test for dogs" approximately in the middle of the page.

**3.**      
  Scroll down a bit on the page and press "Order genetic test now" (in red).

**4.**     
  A new page will appear where it says  
 "Online order".  
There you choose 1. Select species: Press Dog (at the top of the menu bar).

**5.  
**  New page will appear.  
There you choose 2. Select breed: By dragging the red cursor to the right you will get to "Danish-swedish farmdog". There you press the name so that it is highlighted.

**6.**       
 A new page will appear where you will see that it says 3. Select genetic test. There you mark the box for "Primary Lens Luxation (PLL)".

**7.**      
  Cost 58 euros.

**8.**     
  Depending on what you have agreed with your veterinarian, you may also need to order transport material. In that case: Scroll to the bottom of page 4. check the box for "Do you need shipping materials".

**9.**     
  Then go to the top of the page. It says "my account". Log in/register print there and follow the instructions.

**10.**   
 Don't forget to enter the shopping cart and pay for your test.