Source: [Arbetsgruppen för avelsfrågor | gardshund.com](https://gardshund-com.translate.goog/avel/avelarbetsgruppen-f%C3%B6r-avelsfr%C3%A5gor-35478601?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)


## THE WORKING GROUP FOR BREEDING ISSUES

The overall objective is to preserve the Danish-Swedish farm dog as a healthy, alert, capable, friendly and accessible farm and companion dog with the (since long documented) breed-typical exterior.

The breed club works to ensure that the breeding of the Danish-Swedish farm dog takes place in a healthy and genetically sustainable way. Compared to many other dog breeds, the farm dog is healthy and fit.  
  
RDSG works to ensure that breeding is carried out in accordance with the special breeding recommendations and the Breed-Specific Breeding Strategy (RAS) developed within the club.  
  
Breeding must be carried out without interbreeding with other breeds. Only physically and mentally healthy animals should be used as breeding stock.  
  
Inbreeding in the stock must be kept low. When calculating the degree of inbreeding in individuals, all known lineages should be taken into account.  
  
**Members of the working group:**  
Ewa Osterman, 070-512 61 17  
Anna Tjäder, 076-785 78 31  
Anna Dahl, 073-312 41 22   
Gabriela Marsh, 073-687 60 18

  
If you have questions about the work within the group for breeding issues, or ideas for further work, please contact:  
**arbetsgruppavel@gardshund.com**