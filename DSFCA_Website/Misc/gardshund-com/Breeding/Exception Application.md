---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

# 202304220746

## DISPENSATION APPLICATION

For those who have questions about breeding or departures from current breeding recommendations and want to apply for a dispensation, the following applies (2013-03-11).

The processing time for a dispensation is two to four weeks. If additions are required because information is missing in the application, the processing time can be extended. Planned measures may not be started before RDSG's board has decided on the matter (dispensation must be applied for before pairing).  
  
Exemption applications are sent to RDSG's board via e-mail to  [_**styrelsen@gardshund.com**_](mailto:styrelsen@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)