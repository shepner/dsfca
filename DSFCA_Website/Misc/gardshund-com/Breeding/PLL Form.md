---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

Source: [rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/avel/pll-formul%C3%A4r?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

# PLL Form

#### 

To investigate how common the gene that carries PLL is, the breed club/breeding committee will find out how many dogs of the breed carry the gene.

During 2020/21, we hope to receive as many test results as possible.

The breeding committee will compile and evaluate the results and the aim is to be able to find out what percentage of carriers of PLL there are in the breed. As soon as a reliable result is available, everyone will be able to share it. Only when we know how it is, can we set up a strategy going forward.

_A list of PLL-tested dogs can be found_ _**[here](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://55b558c7-site.public.sitebuilder.systems/avel/pll-38129443)**_

[Info After Mapping](http://files.builder.misssite.com/3a/85/3a8554a8-0eb4-4703-8fdc-8530550cb793.pdf)

---

#### 

[Form PLL Results](https://form.jotform.com/210673900530043)

**From January 6, 2022,  
  
 the report you submit must only apply to  
clear, bearer or affected.**

**Hereditary exemption is not reported.**
