Source: [rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/avel/hanhund-till-avel?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

## MALE DOG FOR BREEDING

[Form for the male dog list](https://form.jotform.com/210302652150034)

[Male dog list](Male%20dog%20list.md)

**MY MALE – A BREEDING DOG?**  
The article was written by Åsa Lindholm in 2010 on behalf of SKK/AK.  

Successful breeding work is based on functional, pleasant, healthy and beautiful dogs being chosen as parent animals for the next generation. The female and the male dog both play an important role. If you are a satisfied owner of a good male dog, it is not unusual that you eventually start thinking about whether he could become the father of a litter of puppies. Perhaps you have even been asked by a bitch owner if it is possible to borrow him for a mating.  
  
In dog breeding, it is always the female owner who chooses a suitable breeding male for her next litter. Male dogs that are successful at tests and shows are often in demand. It is not only their profits that make them attractive, the fact that they have been out and shown has allowed bitch owners to see them in several different contexts and form their own opinion of their merits and perhaps also flaws. Males that are not merited in any way are easily forgotten.  
  
On the other hand, awareness of the importance of genetic variation in dog populations has grown in recent years. Matadora breeding, that is, a single male dog fathering a large number of puppies, is an outdated breeding model. This means that good male dogs that have not been used in breeding, that may have a pedigree different from most others, may be of interest to breeders.  
  
Bitches owners and breeders have a great responsibility to ensure that dog breeding is carried out in a good way. But - as a male dog owner, you have that too! Your male dog will, to the same extent as the females, influence the development of the breed and before you lend your male, it is good if you prepare carefully. It's about being inquisitive and being honest, it's about deepening your knowledge and taking the consequences that the increased insight can bring. Regardless of how nice and nice your male dog is, it may happen that you come to the conclusion that he does not measure up as a breeding dog and that you should therefore not lend him out.

## Where do you find the information you need?

One tip is that you start by familiarizing yourself with the Breed-Specific Breeding Strategy, RAS, for your particular breed of dog. It is available on the breed club's website. In RAS, the club has worked out a current picture of the breed, they have set goals for breeding and formulated a strategy to get there. Consider whether your male dog and the litter of puppies he may father are in line with the strategy and contribute to bringing the breed a small step closer to the goal.  
  
You must also know the Kennel Club's Basic Rules. All members of SKK are obliged to follow these rules. Here are general rules about keeping dogs, but also, for example, sections on breeding ethics.  
  
The Kennel Club has a Breeding Policy that provides good guidance on how breeding should be conducted in order to be "targeted, long-term and sustainable", which is the basic idea of ​​SKK.  
  
It is also important that you read the registration rules that apply to your particular breed. Different breeds have different rules for puppies to be registered. For the breeds that are part of health programs, e.g. HD - or eye programs, it applies that the breeding animals must have an examination result determined before mating. This is logical: since the breed has a health program, knowledge of the disease/defect is an important aspect in the selection of breeding animals and one should therefore have a result before deciding which dogs to use in breeding. Check regulations as close to a potential pairing as possible as they do change.

## More sources of information

Each special club has breeding officials who have a good overview of the breed's population and can provide you with valuable information. In the past, breeding councils often had the task of recommending male dogs to female owners, but that role has increasingly been abandoned. Instead, one strives to have a holistic perspective and to be able to inform, for example, a female owner about what is important to consider in breeding for the specific breed in question. Some clubs, however, have male dog lists to help female owners looking for a male. The breeding officer can tell you what you should think about as a male dog owner and advise you on what questions to ask regarding health etc. to the female dog owner.  
  
The vast majority of breed clubs offer their members puppy referrals. Find out what requirements are placed on the parent animal so that the litter can be passed on via the breed club even before you lend your male dog.  
  
There is a service on SKK's website that has been specially developed to facilitate the breeding work. It is called SKK Avelsdata and costs nothing to use. Breeding data has information on breeds both at population and individual level. This means that you can study the competition, test and veterinary examination results of individual dogs, but also that you can easily get statistics of an entire breed. In addition, the breeding data has a function for trial mating between two dogs. By using it, you can quickly get information about the inbreeding rate for a given combination. SKK advises against individual pairings with a higher inbreeding coefficient than 6.25%, i.e. where the dogs are cousins ​​or even closer relatives. For a breed as a whole, the same figure should not exceed 2.5% on an annual basis.  
  
Your own breeder, i.e. the person who raised your male dog, probably also has great and valuable knowledge about your rooster's family picture, both in terms of advantages and weaknesses. The breeder can advise you on what you should watch out for, perhaps on which combinations may entail a risk of various problems. It is also important that you find out as much as you can about the bitch and her family. Should the litter of puppies have, for example, health problems, it will affect your male dog's future breeding value to at least as high a degree as the female's.  
  
The kennel consultant in your county is another resource that you can enjoy. The consultants visit SKK's members, primarily the breeders, to give advice and support, and to ensure that the dogs are well. If you have questions about breeding, keeping dogs or writing contracts, the consultant can help you. Contact details for the consultants in your county can be found at skk.se.

## More though…

Getting all the information is important, but ultimately a breeding decision is about your own dog. Try to look at your male dog objectively. Is he a completely healthy dog? Does he have good mentality? HD programs and eye lights are all fine, but what do they matter if you have a dog that has constant itching or stomach problems? Who may be afraid when he meets strange people or refuses to walk on slippery floors? Puppies not only inherit their parents' preferences (and they don't inherit test scores or competition wins at all!), negative traits are just as likely to be passed on to the offspring.  
  
It can be difficult to determine for yourself which qualities one's male dog has, or lacks. For that reason, it is wise to also consider the opinions and statements of more experienced people. One or two visits to a show usually give a hint about the dog's exterior. Perhaps the special club arranges some kind of aptitude test for the breed's utility characteristics that you can take part in. For working dog breeds, it is now mandatory that the parent animal has passed the Mental Description Dog, MH, in order for the litter of puppies to be registered. For other breeds there is Behavior and personality description dog, BPH. Conducted BPH gives a good picture of the breeding animals' behavior in different everyday situations. A test merit in the parent animal means that the breeder can register the litter of puppies at a discounted price.  
  
If your male dog is not exhibited, he is required to have a veterinary certificate that both testicles are in place in the scrotum in order for a litter of puppies from him to be registered. That certificate must be issued after he has completed six months.  
  
Within certain breeds, there are health programs that limit the use of male dogs, which means that a male dog can only sire a certain number of litters. It may be an idea to only let him mate a couple of bitches at first, because if the result is good, you probably want to have a number of matings saved for later occasions.  
  
There are also other reasons to take a break after a few hills. Not until the puppies are older can they undergo the mandatory health examinations for some breeds and it is only when they have grown up that you get a clearer picture of their health and mentality and the opportunity to evaluate the breeding results. Should it turn out that your male leaves, for example, an abnormally high percentage of hip joint defects, several puppies with allergies or other defects or mental problems, he should no longer be used in breeding.

## Agreements and fees

Before a mating, the owner of the male dog and the owner of the female dog agree on compensation. SKK has drawn up a pairing agreement where you write down your agreement and both parties sign the agreement. There is an old tradition that equates the mating fee with the price of a puppy, but in general it can be said that the mating fee has not followed the price development. Talk to the club's breeding officer about what a mating usually costs. A male dog owner can theoretically set any price for his male dog's services, but if it is unreasonable, even the most interested female owner will probably back out. As a rule, most people can imagine paying a little more to be able to use a well-qualified male dog.  
  
It is common for the mating fee to consist of two parts: a jump fee that you pay at the time of mating and a fee for each puppy. Some male dog owners prefer a lump sum regardless of the number of puppies. A common arrangement is that the female dog owner pays the mating fee in connection with the litter being registered and before the male dog owner signs the registration application. You can find more about agreements and settlements in SKK's booklet "Avtal-bra för alla".  
  
Once again: If you have a dog of a breed that is part of a health program, check carefully that your male (and of course the female in question) has undergone the relevant examinations before mating. Health programs for eyes usually require an annual eye examination - the latest examination date is recorded in SKK Breeding Data.  
  
Also remember that you, as a male dog owner, always have the opportunity to say no to a pairing that, for whatever reason, you feel insecure about.

## Mating

If everyone is confused about mating, you can read in the book "Dog breeding in theory and practice" (printed in 2015). It provides a broad and in-depth knowledge of everything related to dog breeding.  

## Follow-up

Often it is the female owner who keeps in touch with the puppy buyers after the sale. But also as a male dog owner, it can be interesting to follow the little ones out into the world. A good contact with the puppy buyers enables you to follow up and evaluate your male dog's offspring. What did they inherit from their parents, in terms of appearance and characteristics? Are they healthy and sound, are their owners happy with them? In short, did it turn out as you expected?  
  
In summary, you as a male dog owner, just like the female owner, have great responsibility for the product - the puppies - that you produced.  
By using good judgment and trying to look objectively at your male as a breeding dog, you can positively contribute to the development of the breed.  
  
If you have further questions, you are welcome to contact SKK.
