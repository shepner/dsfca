
Source: [Avelsrekommendationer | gardshund.com](https://gardshund-com.translate.goog/avel/avelsrekommendationer-35478600?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

_BREEDING RECOMMENDATIONS_  

## 

_Updated 2022-04-25_

In the work with RAS, a review of the breeding recommendations has been made and these have been modified somewhat in the light of

this work. The breeding recommendations that now apply to the breed are described below. The Danish-Swedish Farm Dog Breed Club (RDSG) has adopted a goal for the breeding business as well as recommendations for how it should be conducted to promote genetic soundness. Furthermore, the Swedish Kennel Club's (SKK's) basic rules for dog breeding apply.  
  
   
The overall objective is to preserve the Danish-Swedish farm dog as a healthy, alert, capable, friendly and accessible farm and companion dog with the long-documented breed-typical exterior. The breeding work must be carried out so that the breed-typical characteristics are preserved and the genetic variation possessed by the breed is not depleted. Breeding must be carried out without crossing other breeds. Only physically and mentally healthy animals should be used as breeding stock. Inbreeding in the stock and the loss of genetic variation must be minimized.  

  

**Minimized inbreeding**

Breeding animals must be chosen so that the inbreeding rate of the offspring is as low as possible and must not exceed SKK's

recommended 6.25 percent. When calculating the degree of inbreeding in individuals, five ranks is a minimum.

  
**Maintained genetic variation**  
Breeding should be planned so that the loss of genetic variation does not occur unnecessarily quickly. The aim should be to increase the proportion of healthy individuals in the stock that have puppies. Older, healthy males that have not yet reproduced should be prioritized in the breeding work. Bitches that have not yet had puppies should be prioritized. Genetic variation is lost with rapid generational changes – that is, when parent animals are young. The female must be at least 24 months old when she is mated for the first time, the male must be at least 24 months old but may well be significantly older so that his characteristics can be evaluated and rapid generational changes are avoided.  
  
**Number of offspring per breeding animal**  
Breeding where individual individuals produce many offspring must be avoided because such so-called matadora breeding constitutes one of the greatest genetic threats to dog breeds. An individual male or female dog must not leave more than 50 puppies. It is also important to follow the extent to which offspring are used in breeding. An overuse of individuals who are closely related should be avoided. Such breeding risks spreading harmful, recessive (declining) traits in the population and reducing the genetic variation. Bitches may have a maximum of 5 litters (in accordance with SKK's regulations). Mating between the same female and male must not leave more than 10 offspring. More offspring on average per male dog compared to the average of bitches should be avoided. An overrepresentation of offspring from one sex generally increases the rate at which genetic variation is lost.  
  
**Health requirements** **General health requirements**  
Only mentally and physically healthy animals may be used in breeding according to SKK's basic rules (breeding ethics) § 2:1–2:7. In the case of defects and diseases where the inheritance is judged to be autosomal recessive and a DNA test validated for Danish-Swedish farm dogs is not available, the following applies: Dogs with the disease/defect must not be used in breeding. Dogs with one or more full siblings exhibiting the disease/defect must not be used in breeding. (Full siblings are individuals with the same parents regardless of whether they are born in the same litter or not.) Dogs that have produced offspring with the disease/defect must not be used in breeding. Dogs suffering from diseases/defects that are hereditary or presumed to be hereditary must not be used in breeding. Caution should be exercised when using close relatives of dogs showing the defect/disease in the breeding work. With caution is meant to postpone breeding debut and avoid overuse. Caution also means being very careful when choosing breeding partners for close relatives of affected individuals in order not to risk duplicating any recessive traits.**Also breed with as low an inbreeding rate as possible to reduce the risk of doubling possible recessive disease predispositions.  
  
  
Disease-specific requirements  
  
Primary Lens Luxation (PLL)  
**RDSG would like to have breeding animals DNA tested for PLL and that the results be reported to the breed club's list. Dogs that have PLL carriers or untested close relatives of PLL carriers in their lines and do not have free-tested dogs in the middle line are strongly recommended to be DNA tested. Dogs that have Son Mik's Ydun (S49362/2001) or Kvik (DK17273/99) in their lines and do not have free tested dogs in the middle line are strongly recommended to be DNA tested. Carriers may only be used in breeding together with dogs that are DNA tested free or are hereditary free. If carriers are to be used in breeding, both female and male dog owners must be informed and understand that the offspring are at risk of becoming carriers. When carriers are used in breeding, the puppy buyers must be informed that their dog may become a carrier and that they in turn must DNA test their dog before any breeding

  

**Cystinuria**  
SKK's general advice for breeding in case of disease with unknown inheritance applies. Do not use sick dogs in breeding. Do not repeat the same parental combination that left sick offspring. Take care when breeding siblings, parents and offspring of sick dogs. With caution is meant to postpone breeding debut and to avoid overuse. Caution also means being very careful when choosing breeding partners for close relatives of affected individuals in order not to risk duplicating any recessive traits. Also breed with as low an inbreeding rate as possible to reduce the risk of doubling possible recessive disease predispositions.

  

**Legg Perthes**  
SKK's general advice for breeding in case of disease with unknown inheritance applies. Do not use sick dogs in breeding. Do not repeat the same parental combination that left sick offspring. Take care when breeding siblings, parents and offspring of sick dogs. With caution is meant to postpone the breeding debut and to avoid overuse. Caution also means being very careful when choosing breeding partners for close relatives of affected individuals in order not to risk duplicating any recessive traits. Also breed with as low an inbreeding rate as possible to reduce the risk of doubling possible recessive disease predispositions. 

  

  

**Hip dysplasia (HD)**   
Danish-Swedish farm dog has health program level 1 for hip dysplasia, which does not require X-rays, but X-ray results can be registered centrally with SKK. No special requirements for breeding animals are linked to this level. Dogs with HD:D or HD:E must not be used in breeding. Dogs with clinical symptoms should not be used in breeding, regardless of the degree of HD.  
  

**Amelogenesis imperfecta (tandem enamel hypoplasia)**

SKK's general advice for breeding in the event of a disease with unknown inheritance applies. That is, the same recommendations as at

cystinuria  
  
**Patella luxation**  
Dogs with patella luxation or who are operated on for patella luxation must not be used in breeding.  
  
**Umbilical hernia**  
Dogs with an umbilical hernia of such an extent that the hernia does not disappear and must be operated upon must not be used in breeding.  
  
**Bites**  
Breeding animals must have scissor bites or pincer bites.  
  
**Teeth (congenital** )  
Breeding animals may lack a maximum of three teeth. If one breeding partner is missing 1-3 teeth, the other breeding partner must be fully toothed  
  
**Mentality requirements**  
Only mentally healthy dogs should be used in breeding. Mental description (MH) and behavior and personality description (BPH) of dogs are strongly encouraged.

  

**Exterior requirements**  
Breeding animals must have received at least Very good in quality assessment at an exhibition at the earliest 18 months of age. For dogs exhibited in 2010 or earlier, breeding animals must have received at least a 2:ai quality assessment. Dogs exhibited before 2012-01-01 meet the exterior requirements if they have received "Good" in quality assessment.

  

  
**Exception**

Exceptions to the above recommendations can be made if the genetic "profit" of using a particular dog is judged to be very large and outweighs the risks of spreading predispositions that produce physical, mental or exterior defects.  
If you wish to use a dog that does not meet the recommendations, a written request must be made to the breed club, which then, in consultation with the breeding council and breeding committee, determines what is serious or tolerable in relation to the genetic gain with the specific individual that is desired to be used.

  
  

**Requirements for assistance to breeders with puppy brokering, breeding advice, advertising.**  
The breed club has a comprehensive service for breeders in terms of help in finding suitable buyers (puppy brokerage), breeding advice, advertising of litters of puppies on the club's website. All this service is free of charge.  
Breeders are also allowed to advertise in the club magazine at a low cost, opportunity to give gift membership to puppy buyers at a heavily subsidized fee (half the fee which in practice means cost price), opportunity to buy informational material at a subsidized price, and more.  
In order to get access to this help, the breeder must follow the breed club's breeding recommendations as well as the ethical recommendations. This means that all dogs (both females and males) to which the breeder has breeding rights (in full or via feed agreement) must meet the breed club's recommendations when they are used in breeding in their own or another person's breeding.  
When it comes to using a male dog in a country outside of Sweden, the relevant country's recommendations apply even if the female does not meet the Swedish breed club's recommendations, as long as the female in question is not Swedish-registered or Swedish-owned and the puppies are not registered in Sweden.  
Furthermore, the conditions under which the dogs are kept must not deviate from the ethical recommendations.  
  
  

  

---

**The working group for breeding issues**

**Members of the working group  
Irene Berglund, 073-182 99 12  
Ewa Osterman, 070-512 61 17  
Anna Tjäder, 076-785 78 31  
Anna Dahl, 073-312 41 22  
Gabriela Marsh, 073-687 60 18  
**
