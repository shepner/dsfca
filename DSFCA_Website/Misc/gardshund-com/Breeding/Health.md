---
source: !
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

Source: [rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/avel/h%C3%A4lsa?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

# Health

!!! TODO "Translate these 2 links"

[Health Program Change SKK/AK NO. 5-2021](http://files.builder.misssite.com/72/dd/72dd3aa3-9640-4d06-9816-7f5be9007a11.pdf)

[Other Health Information | gardshund.com](http://gardshund.com/%C3%B6vrigt-40859681)



**CYSTINURIA  
  

We are currently conducting a study on the relationship between cystinuria and feed and if you want to participate, you can email the breeding group,   
  

[**_arbetsgruppavel@gardshund.com_**](mailto:arbetsgruppavel@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

  

  
  

****_Pregnant - Lactating bitch_**  
  
**

**ECLAMPSIA**   

Eclampsia is a life-threatening condition if the bitch does not get veterinary help.

It mainly affects small and medium-sized breeds and usually occurs when the bitch gives the most milk, that is during the third to fourth week after whelping. However, eclampsia can also occur during the last three weeks of pregnancy and as late as 45 days after the puppy is born.

The cause is not completely known, but the disorder occurs because the bitch has a disturbed regulation of her blood calcium level.

It manifests itself in the bitch having convulsions. The first symptoms are often that the bitch becomes anxious or nervous. She suddenly no longer cares about her puppies. She can drool, gasp and whine. Then she gets involuntary muscle twitches. Within anything from a few minutes up to 8-12 hours, convulsions develop, which become life-threatening if the bitch is not treated by a veterinarian. Often the bitch gets a high fever - up to 41.5 degrees.  
  
The treatment consists in giving the bitch lime. The bitch must then respond quickly to the treatment. If the cramps persist, one can suspect that she is instead or even suffering from low blood sugar. The vigorous muscle activity can often lead to a drop in blood sugar, which also leads to convulsions in the bitch. In those cases, the bitch also receives a sugar solution.

The bitch owner can then continue to treat the bitch at home with calcium supplements for a few days. The puppies are taken from the bitch for 24 hours and fed by hand during that time. Then try adding them again.

If the bitch relapses, the puppies must be weaned. Many times they are at the right age for weaning when the eclampsia occurs. If the bitch gets the right amount of calcium, it is usually fine to add the puppies again, without the bitch having a relapse.

  
Giving the bitch preventive treatment during pregnancy with high doses of calcium and vitamin D is **not** recommended.

However, it is important that the bitch gets a properly balanced feed with a lime/phosphorus ratio of between 1:1 and 1.2:1.

Ewa Osterman/Galant's kennel  
  

Source: canirep.com/valpning/

  

In order to be able to carry out breeding work where the farm dog's health and well-being is the focus, the group for breeding issues needs to receive information about the dogs that are diagnosed with hereditary disease, other diseases or problems that you as a puppy buyer/dog owner experience.

In the case of a hereditary disease, a veterinary certificate must always be sent to  [arbetsgruppavel@gardshund.com](mailto:arbetsgruppavel@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) .  
The Working Group for Breeding Issues strives for an open climate and will use the information submitted in the manner deemed to best benefit our breed.**
