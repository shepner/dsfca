Source: https://www-dsgk-dk.translate.goog/side.asp?ID=27953&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp

### Before you buy a puppy

**Some good advice...     ![](https://www.dsgk.dk//xdoc/293/3322/_260px_293_lille.jpg)**     

 ...to you, who have made it clear to you how much of your time your new companion will take up.

1.  Choose your breeder carefully. You will eventually come to talk to your breeder a lot about how your dog can best have a good life. Therefore, it is very important that you feel safe and welcome when you approach, and that you feel that you will be able to talk to the breeder again and again.  
      
    
2.  Emphasize that the environment your dog grew up in corresponds to the way you want your dog to live for the next 12-15 years. A puppy that is familiar with the interior design of a house will be very easy to keep clean and easy to learn to behave nicely indoors. Such a puppy will also not be afraid of the common activities in a home such as television, radio, vacuum cleaner and washing machine. It has learned that they are not 'dangerous'.  
      
    
3.  It is a good idea to visit several breeders - then as a puppy buyer you can decide where you feel is the best place to buy a dog. You can easily be open to the breeder about the fact that you have been elsewhere. Most breeders are good friends and will find out anyway. If you, as a puppy buyer, choose to 'rescue' a puppy from a terrible place, it will have the opposite effect of your intention - you are supporting the 'bad' breeder by buying a puppy here and thereby giving the 'bad' breeder the courage to breed another litter.  
      
    
4.  Tell the breeder as much as possible about how you imagine a life with a dog. Where will the puppy sleep, are there children in the family, how old are they. What occupation do you plan for your dog, training, show, etc. etc. Be honest. In this way, you and the breeder will best be able to choose the dog that best suits your family.  
      
    
5.  If you think your breeder asks a lot of close-up questions, it is because he/she wants the puppy to come to a home where it will be able to have an exciting, long and good life.  
      
    
6.  The Danish/Swedish Farm Dog Club encourages you to only buy and sell farm dogs with a written purchase agreement. In this way, both parties know what agreements have been made regarding the purchase of a puppy. Your breeder will have lots of good advice for you on how to feed, look after and raise the puppy. Do not hesitate to call the breeder for advice. Most breeders would very much like to follow their puppies for many years, both for the sake of the puppy and to see how its parents pass on their good qualities.  
      
    
7.  Your breeder can enroll you in the Danish/Swedish Farm Dog Club at a reduced price for the first year. If you are interested in this, be sure to tell your breeder.

And then all that's left is to wish you luck with your new dog

**Also read:  
**[](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dansk-kennel-klub.dk/1023)**[The puppy buyer's checklist](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.dkk.dk/k%25C3%25B8b-af-hund/k%25C3%25B8b-hund/k%25C3%25B8berens-tjekliste)** **and**[](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dansk-kennel-klub.dk/439)**[Why buy a dog with a pedigree](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.dkk.dk/k%25C3%25B8b-af-hund/k%25C3%25B8b-hund/hvorfor-v%25C3%25A6lge-en-hund-med-stamtavle)** **on DKK's website**