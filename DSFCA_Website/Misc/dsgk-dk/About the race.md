Source: https://www-dsgk-dk.translate.goog/side.asp?ID=27857&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp

### About the race

  
![](https://www.dsgk.dk//xdoc/293/3336/293_han.jpg)   ![](https://www.dsgk.dk//xdoc/293/3336/293_taeve.jpg)  
  
The Danish/Swedish Farm Dog has had many different names over time: Rat Dog, Danish Fox Terrier or "Fuchsterrier", and in Sweden they called it the Scanian Terrier. However, it is not a terrier at all, it is a pinscher. The farm dog does not have the angry terrier temperament at all.

Farm dogs have been used in the countryside to catch rats and mice, but also for all other incidental work for dogs. They gave up when strangers came, kept the fox from the chickens, went hunting, brought the cows in for milking, played with the children, etc.

In addition to being used as a farm dog, it has also been widely used as a circus dog. What a breed has been used for says a lot about what it is like. If you have to build up a circus act, you use the dogs that learn it most easily. So - Farm dogs are easy to learn, cooperative and happy to learn arts.

In order to catch rats, the dog must be lightning fast and be good at using his nose. In addition, its hunting instinct for small game must be well developed

Here you can read [The breed standard](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=https://www.hundeweb.dk/dkk/public/getRaseFil?FIL_ID%3D849) for Danish/Swedish farm dogs.

Also see  [Breed presentation in English of Danish/Swedish farm dogs](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dsgk.dk/xdoc/293/fci_breed_presentation_danish-swedish_farmdog_2017-2018.pdf)