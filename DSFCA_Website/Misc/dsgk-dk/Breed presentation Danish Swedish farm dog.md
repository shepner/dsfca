Source: https://www-dsgk-dk.translate.goog/side.asp?ID=44377&_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp

### Breed presentation Danish/Swedish farm dog

[![](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dsgk.dk//xdoc/293/3322/_300px_293_forsidenpraesentation_001.jpg)](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://fci.be/Nomenclature/Education/356g02-PRE-en.pdf) The breed's two homeland special clubs, Dansk/Svensk Gårdhundeklub in Denmark and Rasklubban för Dansk/Svensk Gårdshund in Sweden, have jointly prepared a breed presentation in English.

It is one of the biggest tasks that has been solved in a collaboration between the two countries, in the history of the breed. The work has lasted 4 years and has involved many different agencies through the various stages. 

  

Ultimately, the presentation is submitted by the two special clubs to DKK and SKK. Here it has been reviewed and approved by i.a. SKK's judging committee before the two kennel clubs have sent it to the FCI.  

  

The task was set for DKK and SKK by FCI ( [Fédération Cynologique Internationale](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://fci.be/en/) ) all the way back in 2014 and can be found on the FCI website by looking under "Breeds" and "Breed specific education".

  

It can also be downloaded/opened as a pdf file here:

  

[Breed presentation of Danish/Swedish farm dog](https://translate.google.com/website?sl=auto&tl=en&hl=en&client=webapp&u=http://www.dsgk.dk/xdoc/293/fci_breed_presentation_danish-swedish_farmdog_2017-2018.pdf)