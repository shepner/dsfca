---
source: !  # Replace the ! with the URL
tags:
- Travel
---

# Misc

So you found a puppy in Denmark that you want to bring home.  This may help:

>[Gretchen McGrath Vroege](https://www.facebook.com/groups/208781582549857/user/1561233810/?__cft__[0]=AZVlai8bNnhTjFBikvUloBGC_gpyW1HsKRmybYJMHOl2uBbpWAM7dR6vfWg2TvWTUAH9oEswbkgv0NyiSqsMGI1LFKR_uYTSLeocSUf0ow7KIzh235aDoLGRYBLty9SlfOK4LIs9jpn8WAk6HeKJ6VHAzE9R938QqaR-DsaxNOym0cEH3_E9-qY20_Z1L3k7CUk&__cft__[1]=AZVlai8bNnhTjFBikvUloBGC_gpyW1HsKRmybYJMHOl2uBbpWAM7dR6vfWg2TvWTUAH9oEswbkgv0NyiSqsMGI1LFKR_uYTSLeocSUf0ow7KIzh235aDoLGRYBLty9SlfOK4LIs9jpn8WAk6HeKJ6VHAzE9R938QqaR-DsaxNOym0cEH3_E9-qY20_Z1L3k7CUk&__tn__=-UC%2CP-R]-R)
My Dutch husband and I happen to be vacationing in Amsterdam in the Netherlands and just learned about a DSF puppy from [Jenni Kochen Gartnervejens](https://www.facebook.com/groups/208781582549857/user/100000213810968/?__cft__[0]=AZVlai8bNnhTjFBikvUloBGC_gpyW1HsKRmybYJMHOl2uBbpWAM7dR6vfWg2TvWTUAH9oEswbkgv0NyiSqsMGI1LFKR_uYTSLeocSUf0ow7KIzh235aDoLGRYBLty9SlfOK4LIs9jpn8WAk6HeKJ6VHAzE9R938QqaR-DsaxNOym0cEH3_E9-qY20_Z1L3k7CUk&__cft__[1]=AZVlai8bNnhTjFBikvUloBGC_gpyW1HsKRmybYJMHOl2uBbpWAM7dR6vfWg2TvWTUAH9oEswbkgv0NyiSqsMGI1LFKR_uYTSLeocSUf0ow7KIzh235aDoLGRYBLty9SlfOK4LIs9jpn8WAk6HeKJ6VHAzE9R938QqaR-DsaxNOym0cEH3_E9-qY20_Z1L3k7CUk&__tn__=-]K-R]-R) in Denmark that is available. Does anyone have any experience importing a DSF from Denmark to the USA? Our hope is Copenhagen to Amsterdam to Minneapolis.
Please share any experiences. Many thanks!!

[Sue Mills](https://www.facebook.com/groups/208781582549857/user/100002330443984/?__cft__[0]=AZVlai8bNnhTjFBikvUloBGC_gpyW1HsKRmybYJMHOl2uBbpWAM7dR6vfWg2TvWTUAH9oEswbkgv0NyiSqsMGI1LFKR_uYTSLeocSUf0ow7KIzh235aDoLGRYBLty9SlfOK4LIs9jpn8WAk6HeKJ6VHAzE9R938QqaR-DsaxNOym0cEH3_E9-qY20_Z1L3k7CUk&__tn__=R]-R)
I went this past July and brought home a puppy from Jenni. I love her! But you will likely need a direct flight from copenhagen to USA, as you have to meet import standards for each country you land in. I live on the East Coast, so I was able to book a direct flight from copenhagen to dulles. Also, be aware of age requirements on the airline you are flying. Scandinavian air will transport pets as young as 8 weeks. Delta is 16 weeks. Direct message me if you have more questions.

[Casey Roman](https://www.facebook.com/groups/208781582549857/user/1497430154/?__cft__[0]=AZX1Y9IZGxLvCHZL1Aa1kq3gjxLqVQ-3ASSrutW8ps5oMpJrcX2rUFqEuWncn4UPbP8tF7R62E4OS_K_IJzAZlu8tCVxpq85KJq3qt6VhE4lPpVXP58BUn3QGReGSZhU2X7F4ixZwGm4jU1phaxzU9y50dsRCY6DofNP2TJR3ir_Ga2TzLet_qsq75Naqjmt6XA&__tn__=R]-R)
You might want to be prepared to train the puppy to accept the bag while you visit. The Scandinavian airline was more willing to let me hold the puppy in my arms at times. I flew almost 7 years ago.
