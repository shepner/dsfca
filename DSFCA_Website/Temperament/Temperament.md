---
source: !
tags:
- Temperament
---

**Navigation**

- Parent:: [README](../README.md)
- Peer:: [Breeding strategy](../Breeding%20strategy.md), 
- Child:: 

# Temperament

Farmdogs are *very* people oriented and are loving companions.  As in 'never met a stranger' and 'cuddly velcro dog' kind of way.  At a dog park, its entirely possible that your DSF will want to meet (and likely be held by) every person there.  This is also while running around full tilt with all their newfound furry friends.  They will demand attention/affection if they feel deprived.  Dont be surprised if you receive a kissy attack.

Common scenes:

![](_resources/Pasted%20image%2020230401113155.png)

![](_resources/Pasted%20image%2020230401112748.png)

For good or bad, DSFs are rather intelligent, energetic, and tenacious.  This makes them quick learners who are fortunately quite biddable which means they will learn basic obedience quite quickly.  They also pick up on patterns quite quickly as well which can also make them demanding and manipulative little things at times.  Especially if the household isnt following normal routines.  Teaching them to ring a bell on the door to go out is a great but they will also quickly learn how to use that bell to make you do what they want.  ....I really hate that bell at times.

They *do* have a prey drive.  Especially when it comes to rodents.  Dont be surprised if they spend hours chasing that elusive rat or chipmunk.  Or hunting birds, rabbits, insects, etc.  They dig too.  And are surprisingly fast at it.  Giving them a job to do (see[Work](../Work/Work.md) ) will help curb that behavior.  They do coexist fine with cats they have been raised with.

![](_resources/Pasted%20image%2020230401112841.png)

*Very* food oriented as well.  Not (quite) at Beagle levels, but up there so be sure to watch their weight. They can jump remarkably high (repeatedly) which they will happily demonstrate during mealtimes.

Oh yes, they are climbers too:

![](_resources/Pasted%20image%2020230401113014.png)

Fortunately, as a breed, they are not 'barkers'.  But when they do, you will definitely know it as it is extremely sharp and loud when they want to alert you when strangers walk past the house, the mail shows up, or a package is being delivered.  They will quickly stop once you acknowledge the situation.  The play barking isnt nearly as loud.  There is also a growly bark noise that is reserved for something *really* fun.

## Discussions on the subject

[**Alyssa Alheid**](https://www.facebook.com/groups/208781582549857/user/1343447463/?__cft__[0]=AZV8RwDrptDsQZxdTAPGXLUM0X0SjldcLl-n82osnLNRR6D7a3xcaDtRDwYfzVPlnwLuYkxCDqm1jNhlO_ivzzMng9HGHB6lODB5aHEbnVNYT5S4XCLWxpBJRsa4NmTxHQBWtrvXcLYcUJKpyBGkhPrp41gPxXsc9SwFjbH77j0ks-qSx7I-OGdX9MshQ39FCEI&__cft__[1]=AZV8RwDrptDsQZxdTAPGXLUM0X0SjldcLl-n82osnLNRR6D7a3xcaDtRDwYfzVPlnwLuYkxCDqm1jNhlO_ivzzMng9HGHB6lODB5aHEbnVNYT5S4XCLWxpBJRsa4NmTxHQBWtrvXcLYcUJKpyBGkhPrp41gPxXsc9SwFjbH77j0ks-qSx7I-OGdX9MshQ39FCEI&__tn__=-UC%2CP-R]-R):

>Would you consider the DSF to be a biddable, handler-focused breed? Do they want to please you? It’ll be great to hear about this from people who have experience raising, living with and training them. For the record, I currently own a 15mo Whippet, so that’s the level of biddability/focus I’m used to ![😂](https://static.xx.fbcdn.net/images/emoji.php/v9/td0/1/16/1f602.png) Thank you!

[Carey Mabry Segebart](https://www.facebook.com/groups/208781582549857/user/703216274/?__cft__[0]=AZUH8obBQyECkaPiDlC7rkTQoeDY4uzoEBQOos17Iq_4Gl025KTRboxSL039k4w-JzIhvVsaIG_5K9eRkk29riiZFbI9yW_0l_1n14nWrvzQwHpm9tIvtizeosnWF70M2GVHXxtKeMDO8N4B7gnH2dvuNbeRa0OaBgT2CHFUmLvewp7Tqx2ay1hyF2i9svB09V0&__tn__=R]-R): Between my mom and I we have 5 DSFs, 2 boys and 3 girls. There is definitely a difference in temperaments but once you figure each one out they all want to work. I would consider all highly biddable but not obsessive.
My older male is very handler focused unless there’s another dog that grabs his attention. So we work very hard to stay in handler focus as theres not much middle ground with him. That being said he’s a wonderful agility dog, easily got his rally championship just doing local trials, has his CDX in obedience, and success in many other venues. I did not get him until he was 2.5 when he’d never done any formal training, yet he quickly took to everything. He’s the type that needs a job, he is not happy just lounging on the couch. He’ll snuggle up on the couch any time, but gets a little depressed if he’s not working at all.
My young male, son of the other, is about 15 months and a typical goofy boy. But when it’s time to work he gets right down to it. So far he has not been as distractable and his father but I started working him around distractions very early on which is proving beneficial.
With the girls we have one that’s pretty tough and you have to really stay on top of her. As long as you don’t piss her off she’ll work her little heart out for you. But her daughter is extremely soft and hates to be wrong. The third bitch is more in the middle and just wants to work. She likes to do things as fast as she can, so you have to make her slow down and think.
So really it can vary from dog to dog, but in general the breed is very intelligent and very biddable as long as you know what motivates them.

[Alison Smith](https://www.facebook.com/groups/208781582549857/user/1045462608/?__cft__[0]=AZUH8obBQyECkaPiDlC7rkTQoeDY4uzoEBQOos17Iq_4Gl025KTRboxSL039k4w-JzIhvVsaIG_5K9eRkk29riiZFbI9yW_0l_1n14nWrvzQwHpm9tIvtizeosnWF70M2GVHXxtKeMDO8N4B7gnH2dvuNbeRa0OaBgT2CHFUmLvewp7Tqx2ay1hyF2i9svB09V0&__tn__=R]-R): Very much so. Not in the same slavish, unquestioning way that my herding breed does, but they are very bright and very teachable and thoroughly enjoy working together. While there is certainly a lot of variety in this breed in terms of both type and temperament, I have found this to be true in all the farm dogs, I’ve lived with. Over the past 10+ years or so I’ve imported 10 farm dogs who are all obviously their own personalities, but each one is very teachable and engaged. I’ve also lived (sometimes long-term, and sometimes short term)with about another dozen farm dogs . I think the biggest challenge can be, with some of them, keeping their focus when they think they’re following a vole ￼under the roots of a tree! I don’t believe for a minute that they were bred to be circus dogs (as is part of their backstory) but I can imagine them excelling at that job. ￼Happy to chat on the phone to discuss nuances!