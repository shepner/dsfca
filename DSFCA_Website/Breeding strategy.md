---
source: !
tags:
- Breeding_strategy
---

**Navigation**

- Parent:: [README](README.md)
- Peer:: [Breed Standard](Breed%20Standard/Breed%20Standard.md), [Health](Health/Health.md), [Temperament](Temperament/Temperament.md), [Work](Work/Work.md), 
- Child:: 

# Breeding strategy

## Objectives

1. [Temperament](Temperament/Temperament.md): maintain the personality
2. [Breed Standard](Breed%20Standard/Breed%20Standard.md): Ensure future DSFs are directly comparable to current DSFs
3. [Breeding strategy](Breeding%20strategy.md): Maximize genetic diversity
4. [Health](Health/Health.md): Minimize occurrence of disease
5. [Work](Work/Work.md): Ensure they remain a general purpose working breed

## Methodology

**Thoughts/Considerations:**

- What will the state of the breed be in 50, 100, 200 years based upon the above methodology?  
- What will be the likely pain points in maintaining compliance?
- What controls are needed to ensure compliance?
- What are some indicators of a breakdown of the program and/or compliance?
	- sub-breeds
	- inbreeding
	- increase of diseases
	- DSFs no longer looking like DSFs
- How to make people to *WANT* to comply with the program?  (heavy handed rules will only foster resentment and thus non-compliance)

Is it possible to change a Registration from Full to Limited??  Some of the thoughts below would work best if that is possible.

**Ideas:**

(roughly in order of priority)

Ensure the breed remains just as friendly to, well, everything.  Only those who exhibit the expected personality should be in the bloodlines.  Fearful, timid, aggressive, etc is a no-go and there is no need to go into the time/expense of further testing.

The [Breed Standard](Breed%20Standard/Breed%20Standard.md) supports a pretty wide range but those outside of it should not be considered without  good reason.

Diversity

- More breeders with completely unrelated lines
- Limit the number of breedings permitted per animal (ie no *male* OR *female* may breed more than 2-4? times).
- some way to maximize the number of generations apart?
- more diverse genetic transfer between countries encouraged?

Insert plan here for how to ensure diseases are bred out.  Is (or can) this be as simple as just doing DNA tests and not breeding any who are positive?

Ensure this *working* breed stays a working breed:  Those who are able to gain titles, more diverse the better, should be have higher priority in the bloodlines.  While this is last, it is important.  However this is likely the most difficult to enforce and should not be a deal-breaker.

**Based on above:**

The idea here is that DSFs can still be found 'in the wild' so to speak in their native countries.  Since that seems to have worked fairly well on its own for so long, lets try to emulate that:

What if it was encouraged (not mandated, owner+breeder joint discretion) to have 1 breeding per puppy?  what would be the criteria for this?  Seems to make sense to rule out any who test positive for anything undesired.  Namely genetics, personality, and outside the spectrum of the breed standard.  Note that last point is ***NOT*** looking for Conformation title which directly influences the breed by dramatically reduces the gene pool.[^1]

[^1]: The Swedish Farmdog Club has a similar (found here: [Nyheter och information | gardshund.com](https://gardshund-com.translate.goog/avel/nyheter-och-information-37611484?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)) recommendation to not castrate

The DSFCA's awards for AKC titles is a smart move as it helps encourage everyone to keep DSFs a general purpose working breed.  If anything, it might be good to help expand this program to pull in more owners and help them find resources

Testing for temperament seems like a good idea but Im sure how that would best work.  AKC doesnt list too many of those.  Perhaps something at the Conformation shows where 2 other DSF breeders judge this?

Related, the Conformation shows seem to have a bit of a DSF family reunion vibe.  It might be good to encourage this as it could make some of the above a bit easier and would help keep the community closer together.  This may help encourage compliance with the breeding strategy.  Plus the dogs love meeting their extended family...

## Discussion

Based purely upon pictures posted on Facebook, there is already a seemingly noticeable difference between DSFs in the USA vs elsewhere.  There are (based upon observations at shows and Facebook pictures) regional differences within the USA as well.  For example, DSFs in the Midwest tend to be darker.  There also seems to be a strong familial resemblance within the regions.  Likely because many of them *are* related to each other.  Seems like the USA needs more breeders with completely new lines.

---

Some fun exciting news. I ran an initial analysis of genetic diversity in 8 DSF samples that were selected to encompass a diverse group of US DSF. Genetic diversity can be measured using many different methods. It is known that pedigree based diversity measures usually underestimate inbreeding since the number of generations used is too low. Genotype(DNA) based measure allow you to not know the complete pedigrees. This is important with a breed like this one with many different registries like this one.

Genetic diversity is important. All breeds tested have significant loss of diversity and there is no way to regain it with closed stud books. Breeders are encouraged to at least not make it worse if they can.

This lead me to the DSF which has a population history that should make it as diverse as possible.

Enough introduction? Still reading?

They are diverse! Better then any other breed. 1 of 8 had 5% inbreeding and the rest were all 1% or less!! This is so exciting. Cricket is jumping for joy!

---

[farmdogmales.com](http://farmdogmales.com)

---

From [rasklubben för dansk-svensk gårdshund](https://gardshund-com.translate.goog/avel/h%C3%A4lsa?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http):

>In order to carry out breeding work where the health and well-being of the farm dog is the focus, the group for breeding issues needs to receive information about the dogs that are diagnosed with hereditary disease, other diseases or problems that you as a puppy buyer/dog owner experience.

>In the case of a hereditary disease, a veterinary certificate must always be sent to  [arbetsgruppavel@gardshund.com](mailto:arbetsgruppavel@gardshund.com?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http) .  
>The Working Group for Breeding Issues strives for an open climate and will use the information submitted in the manner deemed to best benefit our breed.

It would make sense for DSFCA to adopt a similar program and share information between the clubs

---

From the Swedish Farmdog Club on this subject: [Nyheter och information | gardshund.com](https://gardshund-com.translate.goog/avel/nyheter-och-information-37611484?_x_tr_sl=auto&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp&_x_tr_sch=http)

### _BREEDING CONFERENCE_

#### _Rotebro 20 March 2022_

The breed club for the Danish-Swedish farm dog invited its members to an all-day breeding conference.

There were representatives from about 12 different kennels and several female owners who have plans for future breeding and also several male dog owners who participated. 

It was a combined lecture/discussion session where everyone could contribute their experiences, thoughts and reflections, so recounting in detail will be difficult here, it will be more of a summary of the day in broad strokes. 

The lecture by Maija Eloranta from the Swedish Kennel Club's breeding committee was about, among other things:

- How to choose breeding animals? -with RAS as a practical breeding tool, based on our breed of farm dog.

- How should we preserve the genetic variation?

- How can we increase the breeding base?

- How much genetic variation is there in the breed, and how can we measure and estimate it?   
On average, 49% more females are used in breeding per year than males, proposals were discussed on how to get more different males into breeding, to increase the effective population size and thus reduce inbreeding. That we breeders, for example, would rather use a litter brother or a half brother or maybe even the father of an otherwise popular male dog. How important it is for the breed that many males are kept uncastrated as they can contribute with a litter of puppies when they are older. 

- What is the risk of overuse of individual breeding animals?   
We may end up in new bottlenecks where we lose important genetic variation and diseases that may show up later. If that breeding animal then has many children/grandchildren who inherit the disease, the number of sick animals increases unnecessarily. 

- Age of bitch at first litter?   
That the farm dog is often only mature around 3-4 years, as it matures a little later than many other breeds. 

- How many puppies should a male dog have as a maximum?   
That according to SKK you should then calculate that the puppies born during the male's active years in breeding are a maximum of 5% of the total farm dog puppies born during these years. Assume 4 years in breeding, that 100 farm dog puppies are born in total in Sweden each year, 5% of 400 puppies are 20 puppies and that these 20 puppies are then an absolute upper limit, it is desirable to have fewer puppies per breeding male but that we get more different male dogs in breeding. 

- How harsh or not harsh should we be in our breeding selections considering the genetic variation?  
And DNA tests. Should they be used because they are available or when they are important tools in the breeding work of our breed? Here, together with Maija Eloranta, we came to the conclusion not to test because everything is a fairly sensible approach. And to only use the tests that are validated for the farm dog when they possibly need to be done because otherwise the possible selection in the breeding selection can actually harm more than help. A DNA test does not provide answers to defects or characteristics with complex inheritance, such as allergies, HD. 

- How to choose males for breeding?   
It was discussed how we breeders should find genetically valuable individuals. That now that we have gone down to health program 1 again, we can get a larger selection of breeding animals with less focus on HD, and if we believe it will be used and utilized by the breeders. And what does that entail?

That we should choose males that have relatives with a long life and good health.

With less relatedness, there is a lower probability that the defective genes will be duplicated, thus a chance for healthier dogs. 

- If the inbreeding rate can be underestimated? It is the one in principle in all breeds as SKK's breeding data only calculates on 5 links, and the farm dog database Muttington's calculates on 6 links, but if you take into account all known dogs backwards, each individual's degree of inbreeding is much higher. 

- What should be prioritized in the breeding work? Here we participants came to the conclusion that the most important points are:

  - to breed dogs with a breed-typical temperament that works in today's society.
  - The genetic variation.
  - Longer generation intervals, to dare to breed with older dogs, both bitches and males. 

Maija thought that we breeders, activists and also the breed club should get more information about what a fantastic breed we have which is actually a very healthy dog ​​breed and which is represented in so many different dog sports.

Sofia Westergren


