---
source: !
tags: !
---

**Navigation**

- Parent:: [README](../README.md)
- Peer:: [Temperament](../Temperament/Temperament.md), [Breeding strategy](../Breeding%20strategy.md), 
- Child:: 

# Work

While Danish-Swedish Farmdogs are currently listed in the [AKC Miscellaneous Group](https://www.akc.org/dog-breeds/danish-swedish-farmdog/), they are a general purpose *working* breed.  This means they need a job to do.  Preferably more than one.  The items listed below are just a sampling which is primarily focused upon [AKC](https://www.akc.org) so do not feel limited to them.  DSFs are really versatile little dogs.

See also: [Temperament](../Temperament/Temperament.md)

!!! note  "a report of AKC titles for DSFs would be really handy here"

- [Barnhunt](https://www.barnhunt.com/index.html)
- [Obedience](https://www.akc.org/sports/obedience/)
- [Agility](https://www.akc.org/sports/agility/)
- [Flyball](https://www.akc.org/sports/title-recognition-program/flyball/)
- [FastCAT](https://www.akc.org/sports/coursing/)
- [Herding](http://www.ahba-herding.org)
- [Dock Diving](https://www.akc.org/sports/title-recognition-program/dock-diving/)
- [Search and Rescue](https://www.akc.org/sports/title-recognition-program/search-and-rescue/)

## Discussions on the subject

[**Chris Labatt-Simon**](https://www.facebook.com/groups/208781582549857/user/100000048974328/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__cft__[1]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=-UC%2CP-R]-R):
>We do have a few questions we're hoping people can answer for us. My wife is an athlete and runs between five and eight miles three or four times a week. Our Jack loved it. How much do DSFs enjoy running distances? We also live on our sailboat for four to six weeks during the Summer. Our Jack enjoyed the dinghy rides and swimming at the beaches, plus the hikes we'd do around towns to explore them. How well do you think a DSF would take to this? We want to make sure our new family member, if it ends up being a DSF, enjoys doing the things we do.

>Understanding that the DSF is a recovering breed, what are the expectations of new owners? We've generally rescued puppies and are used to mandatory neutering as part of the rescue. Is there an expectation to NOT do this and encourage continued expansion?

[Irene Meyer](https://www.facebook.com/groups/208781582549857/user/1047339828/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=R]-R): I m curious to hear what people say about their endurance. This is one area where my 20 month old DSF is very different from my Parson Russell Terrier. The DSF gives her all too, but she has much less energy reserves and does slow down earlier. She recovers quickly. But really is a stop and go dog, not an endurance athlete so far. The terrier kept going even in situations without arousal. I think that s what contributes to her off switch. 100% ready and 100% invested .. and then happy to chill until we do something again. The terrier would keep pushing ..

Hiking with sniffing, she can do for long times .. swimming to retrieve she loves. Riding on moving objects in general she likes .. car rides though not at all

[Teri Ann](https://www.facebook.com/groups/208781582549857/user/100000919459761/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=R]-R): [Irene Meyer](https://www.facebook.com/groups/208781582549857/user/1047339828/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=R]-R) excellent explanation! That's how mine is too regarding the endurance. My 9 year old Jack mix keeps going and going whereas my DSF goes hard then naps but then is right back up again and ready to go.

[Dae Grodin](https://www.facebook.com/groups/208781582549857/user/766810172/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=R]-R): [Irene Meyer](https://www.facebook.com/groups/208781582549857/user/1047339828/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=R]-R) I have both a PRT, Border Terrier, and DSF. I find my DSF matches well to my PRT actually both in stamina and continuously going whether aroused or not. Both can chill easily, though the DSF is busier but also much younger. So I think it is still going to be a "it depends." My Border Terrier would tire out much more in general but his temperament is also really different.

[Helene Riisgaard Pedersen](https://www.facebook.com/groups/208781582549857/user/1102928618/?__cft__[0]=AZWxnEwpJL3TQy1f0UKrfkwnH9hx7sTtltkQEGIGDVdrKXuWMXc6Epa70Slke9DGf6-OPP4TFoSah6tTyu1xD7B3XlxgweIvrBcWwxYlKiS20y4C674PLIMgpapN61OZS5BDtM5phwTBbBrnQPSqwLci9vwJ6vIpTh2Dz1arJLweiJhJ9DbzR0LQq0BmdQ6DWMM&__tn__=R]-R): It's not a race dog. It enjoys doing what you do, solely because it loves to please it's human and do things together. It excells in whatever it is encouraged to do for the same reason. But running 5-8 miles is truly not the object of a farmdog. Read about the breed's origin, think about the type of dog performing those tasks, read the breed standard where you find that it as a number one (over all) MUST be "small and compact", have body of "substance", and "elegant appearance" is a fault. The correct farmdog was never constructed for running just for the sake of running. It's consctructed to have a leg in each corner to strattle the rafters on the hay loft and be flashing fast (for just a split second) to go for the catch and kill. It putzed around the farm it did not racetrack around on the farm. And it certainly didn't stray out of the farm to do races in the neigborhood. For running it's great, if it's jogging once in a while, and it can certainly also run 5-8 miles, and will do so, if you ask it to. But truly, inside of my Danish head, it's more for a different type of dog, if that is an important purpose of the family's dog.
