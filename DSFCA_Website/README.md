!!! WARNING "This is not the official website"

    If you wish to learn more about these wonderful little dogs, please goto the [Danish-Swedish Farmdog Club of America home page](https://dsfca.clubexpress.com/content.aspx?page_id=0&club_id=459025)

## The Danish-Swedish Farmdog  

Originally known as a Danish Pinscher. This little dog could be seen on the farms of both Denmark and Southern Sweden. The Farmdog was saved from extinction by the joint effort of the kennel clubs of Denmark and Sweden in 1987. Outgoing and versatile, the Farmdog loves to work and enjoys a challenge. They have excelled in a variety of dog sports and activities including obedience, agility, flyball, K9 Nose Work, lure coursing, and tracking. They perform well as therapy and companion dogs and so much more.

## Personality Plus

[Temperament](Temperament/Temperament.md) and [Work](Work/Work.md):  Farmdogs are *very* people oriented and are loving companions. They need their exercise and can be tenacious in their pursuit of the elusive rodent, snake, bird, leaf or grasshopper, but they have that all important “off switch” and will take that nap, after their work is done!  When they are brought into the family as a puppy, with the proper training and family life, they can be and will do almost anything.

## Appearance

[Breed Standard](Breed%20Standard/Breed%20Standard.md): Danish-Swedish Farmdogs are often mistaken for terriers, but their shape is more rectangular, and their structure is more angulated than most terrier breeds. And as we like to emphasize "They do have an *OFF* mode".  As an owner, you will be constantly asked "what is that" by dog people and if that is a \<fill in the blank> terrier or Beagle by everyone else.

## Health

[Health](Health/Health.md) and [Breeding strategy](Breeding%20strategy.md): On the whole, DSFs are a healthy breed with a low Coefficient of Inbreeding (COI) and the Danish-Swedish Farmdog Club of America has a vested interest in keeping it that way.  There is a strong desire to avoid the pitfalls that have occurred to other breeds.

## Other DSF clubs

The official Danish Farmdog Club: [Dansk/Svensk Gårdhundeklub](https://www.dsgk.dk/)

The official Swedish Farmdog Club: [Startsida | gardshund.com](http://gardshund.com/)
