---
source: !
tags: !
---

**Navigation**

- Parent:: 
- Peer:: 
- Child:: 

# Occurrence

| key | value         |
| --- | ------------- |
| 0   | Not expected  |
| 20  | Very Uncommon |
| 40  | Uncommon      |
| 60  | Frequent      |
| 80  | Very frequent |
| 100 | Expected      |

