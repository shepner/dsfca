---
source: !  # Replace the ! with the URL
tags: ! # Remove the `!` and use [Tag strategy] to help fill out this section
---

# {{title}}

!!!WARNING "Example text here!  Change as appropriate"

This is a paragraph with a  Markdown footnote example[^1]

[^1]: See: Note that Markdown links **MUST** have some plain text between them and the footnote marker in order to work

This is a ***new*** paragraph with *some* additional <u>formatting</u> examples.

Be sure to place a blank line between paragraphs.

<details><summary>Click to display this hidden text example</summary>

Here is some Markdown text that is hidden within a HTML block.  **Notice**: In order for the *formatting* to work, there needs to be a blank line between the HTML and the Markdown

```
codeblock
example
here
```

Some more random text

</details>
